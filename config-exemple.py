#    _________            ____________                                                     ______                        
#    __  ____/_______________  __/__(_)______ _      _________  _______ _______ ______________  /____   _____________  __
#    _  /    _  __ \_  __ \_  /_ __  /__  __ `/_______  _ \_  |/_/  __ `/_  __ `__ \__  __ \_  /_  _ \  ___  __ \_  / / /
#    / /___  / /_/ /  / / /  __/ _  / _  /_/ /_/_____/  __/_>  < / /_/ /_  / / / / /_  /_/ /  / /  __/____  /_/ /  /_/ / 
#    \____/  \____//_/ /_//_/    /_/  _\__, /        \___//_/|_| \__,_/ /_/ /_/ /_/_  .___//_/  \___/_(_)  .___/_\__, /  
#                                     /____/                                       /_/                  /_/     /____/   

#    ________                            ________     ______                                  
#    ___  __/__________ _______ ___      ___  __ \_______  / ________________________ _______ 
#    __  /  _  _ \  __ `/_  __ `__ \     __  / / /  _ \_  /  _  __ \_  ___/  _ \  __ `/_  __ \
#    _  /   /  __/ /_/ /_  / / / / /     _  /_/ //  __/  /___/ /_/ /  /   /  __/ /_/ /_  / / /
#    /_/    \___/\__,_/ /_/ /_/ /_/      /_____/ \___//_____/\____//_/    \___/\__,_/ /_/ /_/ 


##########################################################################################################################
#                                 Use this file as an example to create your own config.py                               #
#                                Change paths and then copy and rename the file to config.py                             #
##########################################################################################################################
 
from Utility import DataType
 
##########################################################################################################################
#                                 Please change variables to suit your own environnement                                 #
##########################################################################################################################
 
PATH_TO_MODELS = "C:\\Users\\JP\\models\\research"
PATH_TO_CARLA = "C:\\Users\\JP\\Stage"
PATH_TO_EGG = "C:\\Users\\JP\\stage\\Carla\\PythonAPI\\carla\\dist\\carla-0.9.9-py3.7-win-amd64.egg" 
PATH_TO_DECISION_MODEL = "C:\\DeepDriver\\Repo\\ML_models\\test.h5"
PATH_TO_TRAFFICLIGHT_MODEL = "C:\\Users\\JP\\Stage\\DeepDriver\\ML_models\\alpha-20-batch8.h5"
 
CARLA_HOST_ADDRESS = "localhost"
CARLA_HOST_PORT = 2000
CARLA_TRAFFIC_MANAGER_PORT = 8000
CARLA_CLIENT_TIMEOUT = 10
 
DESTINATION_SENSOR_FILE = "C:\\Users\\JP\\Stage\\DeepDriver\\Sensors_data"
ML_MODELS = "C:\\Users\\JP\\Stage\\DeepDriver\\ML_models"
 
SENSORS_IMAGE_WIDTH = 600
SENSORS_IMAGE_HEIGHT = 600
NEXT_WAYPOINT_DISTANCE = 2.0
STEP_TIME = 1.0
 
##########################################################################################################################
# --- Warning do not change car model without changing sensor X and Z positions accordingly ---
CAR_MODEL = 'model3'
SENSOR_X_POSITION = 2.5
SENSOR_Z_POSITION = 0.7
 
SENSORS = [
 {
 'TYPE': 'sensor.camera.rgb',
 'ROLE': 'front_car_image',
 'DATA_TYPE': DataType.PICTURE,
 'X_POS': 2.5,
 'Z_POS': 0.7
 },
 {
 'TYPE': 'sensor.other.gnss',
 'ROLE': 'gps',
 'DATA_TYPE': DataType.GPS,
 'X_POS': 2.5,
 'Z_POS': 0.7
 }
]