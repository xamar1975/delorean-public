
print( "\n--Loading libraries :\n" )

import time
import config
import cv2
import numpy as np
import threading
import sys

sys.path.append( config.PATH_TO_EGG )
import carla

print( "All libraries loaded successfully !\n" )

print( "--Loading Classes :\n" )

from Car.Control.Concrete_implementation import CarlaCar
from Car.Sensors.Concrete_implementation import CarlaSensor
from Environments.Concrete_implementation.CarlaEnvironmentControler import CarlaEnvironmentControler
from Environments.Concrete_implementation.carlaClientConnector import CarlaClientConnector
from Utility import ImageProcessors
from Utility import DataType

print( "All classes loaded successfully !\n" )

print( "--Starting client connection :\n" )

connector = CarlaClientConnector()
    
if connector.testServerAvailability():

    client = connector.getClientConnection()
    event = threading.Event()

    spawnpoints = [ 0, 1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240 ]
    cars = []

    for i in range( len(spawnpoints) ):
        print( '--Currently collecting data from spawnpoint #' + str( spawnpoints[i] ) + '\n' )
        
        print('avant')
        
        cars.append( CarlaCar.CarlaCar() )
        cars[i].createCarInClient( client, config.CAR_MODEL, spawnpoints[i] )
        print('ici')
        rgbCamera = cars[i].addSensor( 'sensor.camera.rgb' )
        rgbSensor = CarlaSensor.CarlaSensor( DataType.PICTURE, client, rgbCamera )
        
        print( "--Testing car controls :\n" )

        cars[i].startAutoPilot()
        event.wait(2)
        for j in range(4):
            picture = rgbSensor.getData()

            frame = str(picture.frame)
            print(' Picture #' + str(j) + ' : ' + frame )
            picture.save_to_disk( 'Sensors_data/' + frame + '.jpg' )

            event.wait(2)
            print('test ' + str(i))
        cars[i].removeCarInClient() 
        print( '--Done collecting data from spawnpoint #' + str( spawnpoints[i] ) + '\n' )

connector.terminateConnection()
print( "Fin.\n" )
