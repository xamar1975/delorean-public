import os

# Save the model in the folder specified in the .gitlab-ci.yml (--model_export_path)
def launch( run_data ):
    print( "\n-- Saving model : \n" )
    
    numberOfEpoch = run_data.level_config["number_of_epoch"]
    batchSize = run_data.level_config["batch_size"]
    model = run_data.model

    modelSaveName = f'{run_data.model_export_path}/trafficlights-{numberOfEpoch}epochs-batch{batchSize}.h5'
    #exportPath = os.path.join(run_data.model_export_path, modelSaveName)
    
    print('\nExporting to: ' + modelSaveName)

    model.save( modelSaveName, overwrite=True )

    print('\nModel saved !\n')

    run_data.model = model

    return run_data