import logging


def launch(input):
    run_id = input['RunId']
    client = input['CarlaClient']

    logging.basicConfig(level=logging.INFO, file=f'../self_driving_runs/run_{run_id}/run_{run_id}.log')

    # Create benchmark
    # TODO

    print('creating benchmark for level 01 ...')
    logging.info('creating benchmark for level 01 ...')

    return {'RunId': run_id, 'CarlaClient': client}
