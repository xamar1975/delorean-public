import yaml

# The object that is passed between each steps
class InitTrainingModelData():

    def __init__( self, model_export_path, model_type, config_level, server_config ):
        
        #self.model = 'None'
        #self.history = 'None'

        self.model_export_path = model_export_path
        self.model_type = model_type
        self.config_level = config_level

        self.server_config = server_config

        self.logging_path = f'{self.server_config["logging_path"]}/{self.model_type}'
        # The path to the dataset root folder to train the traffic light model
        self.dataset_path = f'{self.server_config["dataset_path"]}/{self.model_type}'
                
        # load level config
        with open(f'./pipelines/training_environment_setup/config/{self.model_type}/setup_{self.config_level}.yaml', 'r') as setupConfigFile:
            self.level_config = yaml.load(setupConfigFile, Loader=yaml.BaseLoader)