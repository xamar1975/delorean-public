import logging
import time

import yaml

from Utility import DataCollectorLineAngle


def launch(input):
    run_id = input['RunId']
    client = input['CarlaClient']
    car = input['CAR']
    dataCollector = DataCollectorLineAngle(car)

    logging.basicConfig(level=logging.INFO, file=f'../self_driving_runs/run_{run_id}/run_{run_id}.log')

    serverConfig = {}
    with open(f'./server_config.yaml', 'r') as serverConfigFile:
        serverConfig = yaml.load(serverConfigFile)

    dataCollector.CollectData(200, 2, f'{serverConfig["data_output"]}/run_{run_id}')

    # run the car
    car.startAutoPilot()

    print('car is running ...')
    logging.info('car is running ...')

    time.sleep(10)

    return {'RunId': run_id, 'CarlaClient': client, 'CAR': car}
