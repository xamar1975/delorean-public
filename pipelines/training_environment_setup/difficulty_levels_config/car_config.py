from Utility import DataType

CAR_MODEL = 'model3'

SENSORS = [
    {
        'TYPE': 'sensor.camera.rgb',
        'ROLE': 'front_car_image',
        'DATA_TYPE': DataType.PICTURE,
        'X_POS': 2.5,
        'Z_POS': 0.7
    }
]
