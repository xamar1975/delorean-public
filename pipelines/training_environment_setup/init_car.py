import logging

import yaml

from Car.Control.Concrete_implementation import CarlaCar
from pipelines.training_environment_setup.difficulty_levels_config import car_config


def launch(input):
    run_id = input['RunId']
    client = input['CarlaClient']

    logging.basicConfig(level=logging.INFO, file=f'../self_driving_runs/run_{run_id}/run_{run_id}.log')

    # load difficulty level config
    difficultyLevelConfig = {}
    with open('./difficulty_levels_config/difficulty_01.yaml', 'r') as difficultyLevelConfigFile:
        difficultyLevelConfig = yaml.load(difficultyLevelConfigFile)

    car = CarlaCar()
    spawnPoint = difficultyLevelConfig['car_spawn_point']
    car.createCarInClient(client, car_config['CAR_MODEL'], spawnPoint)

    for sensor in car_config.SENSORS:
        car.addSensor(
            sensor['TYPE'],
            sensor['X_POS'],
            sensor['Z_POS'],
            sensor['DATA_TYPE'],
            sensor['ROLE'])

    # TODO Check with pipeline framework how to reuses same car and sensors objects in each steps

    print('car creation done')
    logging.info('car creation done')

    return {'RunId': run_id, 'CarlaClient': client, 'CAR': car}
