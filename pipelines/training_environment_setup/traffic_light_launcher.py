import logging
import yaml
import os
import sys

from pipelines.Exceptions import StepFailedException

from .init_training_model_data import InitTrainingModelData

from . import traffic_light_organize_data
from . import traffic_light_train
from . import traffic_light_artifact
from . import traffic_light_evaluate

def train( model_export_path, model_type, config_level ):
    try:
        print("Input parameters:")
        print("   --model_export_path: " + model_export_path)
        print("   --model_type: " + model_type)
        print("   --config_level: " + config_level)
    
        # Load server config    
        print("Load server config")
        
        server_config = {}
        with open('./pipelines/training_environment_setup/server_config.yaml', 'r') as serverConfigFile:
            server_config = yaml.load(serverConfigFile, Loader=yaml.BaseLoader)

        print("Initlialize training model data")
        run_data = InitTrainingModelData(model_export_path, model_type, config_level, server_config)
   
        # Start the model training
        print('Run is starting...')
        
        print("Organizing data")
        run_data = traffic_light_organize_data.launch(run_data)
        
        print("Model training")
        run_data = traffic_light_train.launch(run_data)
        
        print("Create model artifact")
        run_data = traffic_light_artifact.launch(run_data)
        
        #print("Evaluate and test model")
        #run_data = traffic_light_evaluate.launch(run_data)

        print('Run is over!')

    except Exception as exceptionMessage:
        logging.error( exceptionMessage )