import tensorflow as tf
from tensorflow import keras

# Train the model according to the specified settings (pipelines/config/traffic_light/setup_[0-9]{2}.yaml 
# where [0-9]{2} is the config_level parameter in gitlab-ci.yaml)
def launch( run_data ):
    print( "\n-- Model Training : \n" )

    # Defined in setup_*.yaml
    numberOfEpoch = int(run_data.level_config["number_of_epoch"])
    batchSize = int(run_data.level_config["batch_size"])

    train_generator = run_data.train_generator
    validation_generator = run_data.validation_generator
    model = run_data.model

    print( ' Training model for ' + str(numberOfEpoch) + ' epochs\n' )


    #logdir=model_summary_path # + datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir='pipelines/config')

#    history = run_data.model.fit( 
#        run_data.train_generator, epochs = numberOfEpoch, validation_data = run_data.validation_generator, verbose = 1 )
    history = model.fit( 
        train_generator, 
        batch_size = batchSize, 
        epochs = numberOfEpoch, 
        verbose = 2,
        validation_data = validation_generator,
        callbacks=[tensorboard_callback]
        )

    print( "Model trained !\n" )

    run_data.history = history
    run_data.model = model

    return run_data