import matplotlib.pyplot as plt

# Test and evaluate the model
def launch( run_data ):
    print( "\n-- Testing model : \n" )

    model = run_data.model

    scores = model.evaluate( run_data.test_generator, verbose = 1 )

    print( "\n -- Displaying Model Training and Test Stats : \n" )

    print( " Test Results :" )
    print( " %s: %.2f%%" % (model.metrics_names[1], scores[1]*100) )

    # metrics d'evaluation
    history = run_data.history
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs = range( len(acc) )

    plt.plot( epochs, acc, 'r', label='Training accuracy' )
    plt.plot( epochs, val_acc, 'b', label='Validation accuracy' )
    plt.title( 'Training and validation accuracy' )
    plt.legend( loc=0 )
    plt.figure()

    # affichage
    plt.show()


