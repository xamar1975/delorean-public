import logging

import carla
import yaml

from Environments.Concrete_implementation.CarlaEnvironmentController import CarlaEnvironmentController


def launch(input):
    run_id = input['RunId']
    client = input['CarlaClient']
    car = input['CAR']
    envControl = CarlaEnvironmentController(client)

    logging.basicConfig(level=logging.INFO, file=f'../self_driving_runs/run_{run_id}/run_{run_id}.log')

    print('destroying actors ...')
    logging.info('destroying actors ...')

    car.removeCarInClient()

    # remove benchmark
    # TODO
    print('removing benchmark ...')
    logging.info('removing benchmark ...')

    # Reset weather
    serverConfig = {}
    with open(f'./server_config.yaml', 'r') as serverConfigFile:
        serverConfig = yaml.load(serverConfigFile)

    print('reseting weather ...')
    logging.info('reseting weather ...')

    file = open(f'{serverConfig["self_driving_runs_output_path"]}/run_{run_id}/weatherSnapshot.yaml', 'r')
    envControl.setWeatherFromDict(yaml.load(file))
    file.close()

    print(f'Current weather: {carla.World.get_weather(client.get_world())}')
    logging.info(f'Current weather: {carla.World.get_weather(client.get_world())}')

    print('deleting client')
    logging.info('deleting client')
    del client

    return {}
