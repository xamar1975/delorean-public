import tensorflow as tf

# run_data contains:
#    level_config coming from config/traffic_light/setup_*.yaml
#        dataset_root
#        number_of_epoch
#        batch_size
def launch( run_data ):
    print( "\n-- Version check :\n" )

    print( "Tensorflow version : " + tf.__version__ )

    print( "\n-- GPU check :\n" )

    device_name = tf.test.gpu_device_name()

    if device_name != '/device:GPU:0':
        print( 'GPU device not found' )
    else:
        print( 'Found GPU at: {}'.format(device_name) )

    print( "\n-- Model creation :\n" )

    # classes to differenciate from :
    # red
    # nored

    img_size = 600
    customShape = ( img_size, img_size, 3 )

    model = tf.keras.models.Sequential()

    model.add( tf.keras.layers.Conv2D(32, (3, 3), input_shape=customShape) )
    model.add( tf.keras.layers.Activation('relu') )
    model.add( tf.keras.layers.MaxPooling2D(pool_size=(2, 2)) )

    model.add( tf.keras.layers.Conv2D(32, (3, 3)) )
    model.add( tf.keras.layers.Activation('relu') )
    model.add( tf.keras.layers.MaxPooling2D(pool_size=(2, 2)) )

    model.add( tf.keras.layers.Conv2D(64, (3, 3)) )
    model.add( tf.keras.layers.Activation('relu') )
    model.add( tf.keras.layers.MaxPooling2D(pool_size=(2, 2)) )

    model.add( tf.keras.layers.Flatten() )
    model.add( tf.keras.layers.Dense(64) )
    model.add( tf.keras.layers.Activation('relu') )
    model.add( tf.keras.layers.Dropout(0.5) )
    model.add( tf.keras.layers.Dense(1) )
    model.add( tf.keras.layers.Activation('sigmoid') )
    
    # Defined in setup_*.yaml
    datasetFolder = f'{run_data.dataset_path}/{run_data.level_config["dataset_root"]}'
    trainDir = datasetFolder + '/train/'
    testDir = datasetFolder + '/test/'
    validationDir = datasetFolder + '/valid/'

    print( "\n-- Loading dataset : " + datasetFolder + "\n" )

    # Defined in setup_*.yaml
    batch_size = int(run_data.level_config["batch_size"])

    print( 'Batch size : ' + str(batch_size) )

    # rescale is important to have data from 0.0 and 1.0
    # horizontal_flip=False because trafficlight in Carla are always anchored on the right side of the road
    # validation_split=0.1 could replace the valid subset directory which are randomly picked images
    # rescale is important to have data from 0.0 and 1.0

    # Following augments where tried with no performance improvement
    # zoom_range=0.1
    # rotation_range=20,
    # shear_range=0.2,
    train_image_gen = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255 )

    # always only rescale for the valid et test subsets
    image_gen = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255 )

    train_generator = train_image_gen.flow_from_directory(
            trainDir, 
            target_size=( img_size, img_size ),
            batch_size=batch_size,
            class_mode='binary',
            shuffle=True,
            color_mode='rgb')

    validation_generator = image_gen.flow_from_directory(
            validationDir,
            target_size=( img_size, img_size ),
            batch_size=batch_size,
            class_mode='binary',
            shuffle=True,
            color_mode='rgb' )

    test_generator = image_gen.flow_from_directory(
            testDir,
            target_size=( img_size, img_size ),
            batch_size=batch_size,
            class_mode='binary',
            shuffle=True,
            color_mode='rgb' )

    print( "\n-- Model Summary :\n" )

    model.summary()

    print( "\n-- Model Compilation : \n" )

    model.compile( loss='binary_crossentropy',
                   optimizer='Adam',
                   metrics=['accuracy'] )                

    print( "-- Nodel organized !\n" )

    run_data.model = model
    run_data.train_generator = train_generator
    run_data.validation_generator = validation_generator
    run_data.test_generator = test_generator

    return run_data