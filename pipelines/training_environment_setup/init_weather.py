import logging
import os

import carla
import yaml

from Environments.Concrete_implementation.CarlaEnvironmentController import CarlaEnvironmentController
from Environments.Concrete_implementation.CarlaClientConnector import CarlaClientConnector
from pipelines.Exceptions import ConfigFileErrorException


def intiateWeather(envControl, difficultyLevelConfig):
    if difficultyLevelConfig['weather'] == 0:
        envControl.setToWeatherToNormalDayTime()
    elif difficultyLevelConfig['weather'] == 1:
        envControl.setToWeatherToNormalEvening()
    elif difficultyLevelConfig['weather'] == 2:
        envControl.setToWeatherToNormalNight()
    elif difficultyLevelConfig['weather'] == 3:
        envControl.setToWeatherToRainyDayTime()
    elif difficultyLevelConfig['weather'] == 4:
        envControl.setToWeatherToRainyEvening()
    elif difficultyLevelConfig['weather'] == 5:
        envControl.setToWeatherToRainyNight()
    elif difficultyLevelConfig['weather'] == 6:
        envControl.setWeatherFromDict(difficultyLevelConfig)
    else:
        raise ConfigFileErrorException(f'An error occured with the difficultyLevelConfig file. Difficulty level: {difficultyLevelConfig["difficulty_level"]}')


def launch(input):
    run_id = input['RunId']

    logging.basicConfig(level=logging.INFO, file=f'../self_driving_runs/run_{run_id}/run_{run_id}.log')

    connector = CarlaClientConnector()
    if not connector.testPortAvailability():
        raise Exception("CarlaServer is down")  # utiliser le message de retour de TestServerAvailability

    client = connector.getClientConnection()
    envControl = CarlaEnvironmentController(client)

    # load server config    
    serverConfig = {}
    with open(f'./server_config.yaml', 'r') as serverConfigFile:
        serverConfig = yaml.load(serverConfigFile)

    # prepare directory
    if not os.path.exists(f'{serverConfig["self_driving_runs_output_path"]}/run_{run_id}'):
        os.mkdir(f'{serverConfig["self_driving_runs_output_path"]}/run_{run_id}')

    # save carla current weather in newly created (temporary) file:
    with open(f'{serverConfig["self_driving_runs_output_path"]}/run_{run_id}/weatherSnapshot.yaml', 'w') as Weathervalues:
        yaml.dump(envControl.exportWeatherSnapshotToDict(), Weathervalues)

    # load difficulty level config
    difficultyLevelConfig = {}
    with open('./difficulty_levels_config/difficulty_01.yaml', 'r') as difficultyLevelConfigFile:
        difficultyLevelConfig = yaml.load(difficultyLevelConfigFile)

    intiateWeather(envControl, difficultyLevelConfig)

    print('weather is now :')
    logging.info('weather is now :')

    print(f'Current weather: {carla.World.get_weather(client.get_world())}')
    logging.info(f'Current weather: {carla.World.get_weather(client.get_world())}')

    return {'RunId': run_id, 'CarlaClient': client}
