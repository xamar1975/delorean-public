import logging
import yaml
import os

from pipelines.Exceptions import StepFailedException

from pipelines.training_environment_setup import init_weather
from pipelines.training_environment_setup import init_benchmarks
from pipelines.training_environment_setup import init_car
from pipelines.training_environment_setup import run
from pipelines.training_environment_setup import tear_down_environment

def run_step( step_name, run_callback ):

    try:
        logging.info( f'running step: {step_name}' )
        return run_callback( input )

    except Exception as exceptionMessage:
        raise StepFailedException( f'A problem occured while running {step_name}, internal error message: {exceptionMessage}' )

try:

    logging.basicConfig(level=logging.INFO, file='./self_driving_runs/runs_history.log')

    # load server config    
    serverConfig = {}
    with open('./training_environment_setup/server_config.yaml', 'r') as serverConfigFile:
        serverConfig = yaml.load(serverConfigFile)

    run_id = None
    with open('./self_driving_runs/runs_history.log', 'r') as difficultyLevelConfigFile:
        run_id = 1 # Read from .log file (next run id)
    
    logging.info(f'Run Id: {run_id}')

    if not os.path.exists(f'./self_driving_runs/run_{run_id}'):
        os.mkdir(f'./self_driving_runs/run_{run_id}')

    logging.basicConfig(level=logging.INFO, file=f'./self_driving_runs/run_{run_id}/run_{run_id}.log')

    print( 'run is starting...' )
    logging.info( 'run is starting...' )
    init_weather_output = run_step( 'initializing weather', init_weather.launch( { 'RunId': run_id } ) )
    init_benchmarks_output = run_step( 'initializing benchmark', init_benchmarks.launch( init_weather_output ) )
    init_car_output = run_step( 'initializing car', init_weather.launch( init_benchmarks_output ) )
    run_output = run_step( 'running car', run.launch( init_car_output ) )

    # step to collect data

    tear_down_environment_ouput = run_step( 'tearing down the environment', run_output )

except Exception as exceptionMessage:
    
    logging.error( exceptionMessage )