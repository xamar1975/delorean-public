import os
import sys

carlaHome = os.environ["CARLA_HOME"].replace("\\", "/")
entries = os.scandir(carlaHome + "/WindowsNoEditor/PythonAPI/carla/dist/")

carlaEgg = None
for entry in entries:
    if carlaEgg != None:
        print("Multiple files in 'dist/' directory of carla. Check to cleanup your carla extract")
        sys.exit(-1)

    carlaEgg = entry.name

os.system("easy_install " + carlaHome + "/WindowsNoEditor/PythonAPI/carla/dist/" + carlaEgg)

print("Carla Installed: " + carlaEgg)

sys.exit(0)
