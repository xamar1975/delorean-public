import os
import random
import threading

# necessary to use Carla.XXXXX
import carla

import config
from Car.Perception.Concrete_implementation import ObjectRecognitionMOCK
from Car.Planning.Concrete_implementation.CarlaRoutePlanner import CarlaRoutePlanner
from Car.Sensors.Concrete_implementation import Snapshot
from Utility import RecognitionType, ExportToCSV, misc


class DataCollectorLineAngle():

    def __init__(self, client, carlaCar):  # CarlaDataCollector(totalTime, path, testCar, stepTime)
        self.event = threading.Event()

        self.redLightDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK(RecognitionType.TRAFFIC_LIGHT)
        self.carDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK(RecognitionType.CAR)
        self.pedestrianDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK(RecognitionType.PEDESTRIAN)

        self.carlaCar = carlaCar
        self.client = client
        self.routePlanner = CarlaRoutePlanner(self.client, self.carlaCar)
        self.snapshots = []
        self.lastAngle = 0

    def CollectData(self, totalTime, path):
        runData = []
        waitTimes = [0.1, 0.12, 0.13, 0.15, 0.2, 0.23, 0.24, 0.25,
                     0.25, 0.26, 0.24, 0.27, 0.24, 0.26, 0.28, 0.25,
                     0.25, 0.26, 0.24, 0.27, 0.24, 0.26, 0.28, 0.25,
                     0.20, 0.21, 0.22, 0.23, 0.24, 0.25, 0.25, 0.25,
                     0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33,
                     0.65, 0.70, 0.80, 0.6, 0.68, 0.82, 0.75, 0.6,
                     0.35, 0.35, 0.36, 0.37, 0.4, 0.45, 0.5, 0.55, 0.6]
        self.event.wait(4)
        for i in range(config.NUMBER_OF_SNAPSHOTS_AHEAD + 2):
            self.snapshots.append(self.__recordsnapshot())
            self.event.wait(0.2)

        for i in range(totalTime):
            snapshotWithAngles = self.__addCorrectiveData()
            self.__printSnapshot(i, snapshotWithAngles)

            self.snapshots.pop(0)
            self.snapshots.append(self.__recordsnapshot())
            runData.append(snapshotWithAngles)

            self.event.wait(random.choice(waitTimes))
            # ----Save data in batches of 100----
            if (i % 100 == 0):
                ExportToCSV.ExportDataToCSV(runData)
                runData = []

        ExportToCSV.ExportDataToCSV(runData)

    def __addCorrectiveData(self):

        for i in range(config.NUMBER_OF_SNAPSHOTS_AHEAD):
            lastLocation = self.snapshots[0].location
            currentLocation = self.snapshots[1].location
            currentYaw = self.snapshots[1].yaw
            target = self.snapshots[i + 2]
            targetLocation = target.location

            projectedLocation = misc.projectedLocation(lastLocation, currentLocation)
            magnitude, angle = misc.compute_magnitude_angle(targetLocation, currentLocation, currentYaw)
            direction = misc.calcDir(currentLocation, projectedLocation, targetLocation)

            correctiveAngle = angle * direction
            if correctiveAngle > 170:
                correctiveAngle -= 180
            if correctiveAngle < -170:
                correctiveAngle += 180
            if correctiveAngle > 80:
                correctiveAngle -= 90
            if correctiveAngle < -80:
                correctiveAngle += 90

            if correctiveAngle == 0:
                correctiveAngle = self.lastAngle

            self.lastAngle = correctiveAngle

            self.snapshots[1].correctiveAngles.append([correctiveAngle, magnitude])
        return self.snapshots[1]

    def __printSnapshot(self, tickNumber, snapshot):

        print("\n ------------" + str(tickNumber) + "------------------")
        print("Acceleration     = " + str(snapshot.acceleration))
        print("Direction        = " + str(snapshot.direction))
        print("brake            = " + str(snapshot.brake))
        # print("Is a car in front                = "+ str( snapshot.isCarInFront ))
        # print("Distance to car in front         = "+ str( snapshot.distanceToCar) )
        # print("is a pedestian in front          = "+ str( snapshot.isPedestrianInFront ))
        # print("Distance to pedestian in front   = "+ str( snapshot.distanceToPedestrian) )
        # print("Is a traffic Light in front      = "+ str( snapshot.isTrafficLightInFront))
        # print("Distance to Light in front       = "+ str(snapshot.distanceToTrafficLight) )
        # print("Location                          ="+ str(snapshot.location))
        print("Speed            = " + str(snapshot.speed) + "km/h")
        # print("Yaw                          ="+ str(snapshot.yaw))

        self.client.get_world().debug.draw_string(snapshot.location, 'o', draw_shadow=False,
                                                  color=carla.Color(r=0, g=255, b=0), life_time=3,
                                                  persistent_lines=True)

        i = 0
        for angle in snapshot.correctiveAngles:
            print("Corrective angle    #" + str(i) + "    = " + str(angle[0]))
            print("Distance to next WP #" + str(i) + "    = " + str(angle[1]))
            i += 1

    def __recordsnapshot(self):
        # setup current data
        snapshot = Snapshot.Snapshot()
        drivingData = self.carlaCar.getDrivingData()

        snapshot.acceleration = f"{drivingData[0]:.8f}"
        snapshot.direction = f"{drivingData[1]:.8f}"
        snapshot.brake = f"{drivingData[2]:.8f}"
        snapshot.isCarInFront = (len(self.carDetector.detectObjects(self.carlaCar)) > 0)
        snapshot.distanceToCar = "0.0"  # logic To-Do
        snapshot.isPedestrianInFront = (len(self.pedestrianDetector.detectObjects(self.carlaCar)) > 0)
        snapshot.distanceToPedestrian = "0.0"  # logic To-Do
        snapshot.isTrafficLightInFront = (len(self.redLightDetector.detectObjects(self.carlaCar)) > 0)
        snapshot.distanceToTrafficLight = "0.0"  # logic To-Do
        snapshot.location = self.carlaCar.getVehiculeLocation()
        snapshot.yaw = self.carlaCar.getTransform().rotation.yaw
        snapshot.speed = self.carlaCar.getSpeed()

        return snapshot

    def __collectSinglePicture(self, path):
        image = None

        for sensor in self.carlaCar.sensors:
            if sensor.role == 'front_car_image':
                image = sensor.getData()

                if self.carlaCar.is_at_traffic_light() and self.carlaCar.get_traffic_light():
                    if not os.path.exists(f'{path}/red_light'):
                        os.mkdir(f'{path}/red_light')
                    image.save_to_disk(f'{path}/red_light/%.6d.jpg' % image.frame_number)
                else:
                    if not os.path.exists(f'{path}/not_red_light'):
                        os.mkdir(f'{path}/not_red_light')
                    image.save_to_disk(f'{path}/not_red_light/%.6d.jpg' % image.frame_number)

#     def CollectEnvironmentData( self, totalTime, stepTime, path ):


#         runData = []

#         for i in range(totalTime): 
#             if i==0:
#                 self.data = self.carlaCar.getDrivingData()
#                 self.location = self.carlaCar.getVehiculeLocation()

#             else:    
#                  #setup current data
#                 self.nextData = self.carlaCar.getDrivingData()
#                 self.nextLocation = self.carlaCar.getVehiculeLocation()

#                 # Add stepData to runData And save picture
#                 runData.append(self.__collectSingleEnvironmentData())

# world_snapshot = self.client.get_world().wait_for_tick()


#         for i in range(totalTime): 
#             self.__collectSinglePicture(path)
#             world_snapshot = self.client.get_world().wait_for_tick()
