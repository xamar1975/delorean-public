import threading
import os
import carla
import config

from Utility import RecognitionType
from Car.Control.Concrete_implementation import CarlaCar
from Car.Perception.Concrete_implementation import ObjectRecognitionMOCK

class DataCollector():

    def __init__( self, carlaCar ): # CarlaDataCollector(totalTime, path, testCar, stepTime) 

        self.car = carlaCar
        self.lastData = []
        self.currentData = []     
        self.lastLocation = None
        self.currentLocation = None
        self.redLightDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK(RecognitionType.TRAFFIC_LIGHT)
        self.carDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK(RecognitionType.CAR)
        self.pedestrianDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK(RecognitionType.PEDESTRIAN)

    # def CollectData( self, totalTime, stepTime, path ):
        
    #     runData = self.CollectEnrionmentData(totalTime, stepTime, path)
    #     self.CollectPictures(totalTime, stepTime, path)

    #     return runData


    def CollectData( self, totalTime, stepTime, path ):
        
        event = threading.Event()
        runData = []  
        
        for i in range(totalTime): 
            if i==0:
                self.lastData = self.car.getDrivingData()
                self.lastLocation = self.car.getVehiculeLocation()
            
            else:
                #setup current data
                self.currentData = self.car.getDrivingData()
                self.currentLocation = self.car.getVehiculeLocation()
                
                # Add stepData to runData And save picture
                stepData = self.__collectSingleEnvironmentData()
                runData.append(stepData)
                self.__collectSinglePicture(path)
            
                self.lastData = self.currentData
                self.lastLocation = self.currentLocation
            
            event.wait(stepTime)

        return runData

    def __collectSingleEnvironmentData(self):

        singleData = []
        #tweak steering data 
        if (self.lastData[1] >= -0.001 and self.lastData[1] <= 0.001) :
            self.lastData[1] = 0.0

        lastAcceleration = f"{self.lastData[0]:.7f}" 
        lastDirection = f"{self.lastData[1]:.3f}"
        lastIsCarInFront = (len(self.carDetector.detectObjects(self.car))>0)
        lastDistanceToCar = "0"
        lastIsPedestrianInFront = (len(self.pedestrianDetector.detectObjects(self.car))>0)
        lastDistanceToPedestrian = "0"
        lastIsTrafficLightInFront = (len(self.redLightDetector.detectObjects(self.car))>0)
        lastDistanceToTrafficLight = "0"
        deltaX = f"{(self.currentLocation.x - self.lastLocation.x):.7f}"
        deltaY = f"{(self.currentLocation.y - self.lastLocation.y):.7f}"

        print("Acceleration: " + str( lastAcceleration ))
        print("Direction: " + str( lastDirection ))
        print("Is a car in front: "+ str( lastIsCarInFront ))
        print("Distance to car in front: "+ lastDistanceToCar )
        print("is a pedestian in front: "+ str(  lastIsPedestrianInFront ))
        print("Distance to pedestian in front: "+ lastDistanceToPedestrian )
        print("Is a traffic Light in front: "+ str(  lastIsTrafficLightInFront ))
        print("Distance to Light in front: "+ lastDistanceToTrafficLight )
        print("X ditance to next waypoint: "+ str( deltaX ))
        print("Y ditance to next waypoint: "+ str(deltaY )+'\n')

        singleData.append(lastAcceleration) #accelleration
        singleData.append(lastDirection) #DirectionDirection
        singleData.append(1 if lastIsCarInFront else 0) #isCarInFront
        singleData.append(lastDistanceToCar) #DistanceToClosestCar
        singleData.append(1 if lastIsPedestrianInFront else 0) #isPedestrianInFront
        singleData.append(lastDistanceToPedestrian) #DistanceToClosestPedestrian
        singleData.append(1 if lastIsTrafficLightInFront else 0) #trafficLightInFront
        singleData.append(lastDistanceToTrafficLight) #distanceToTrafficLight
        singleData.append(deltaX) #waypointDistance_x
        singleData.append(deltaY) #waypointDistance_y

        return singleData


    def __collectSinglePicture(self, path):
        image = None

        for sensor in self.car.sensors:
            if sensor.role == 'front_car_image': 
                image = sensor.getData()

                if self.car.is_at_traffic_light() and self.car.get_traffic_light():
                    if not os.path.exists(f'{path}/red_light'):
                        os.mkdir(f'{path}/red_light')
                    image.save_to_disk(f'{path}/red_light/%.6d.jpg' % image.frame_number)
                else:
                    if not os.path.exists(f'{path}/not_red_light'):
                        os.mkdir(f'{path}/not_red_light')
                    image.save_to_disk(f'{path}/not_red_light/%.6d.jpg' % image.frame_number)

        
    def CollectEnvironmentData( self, totalTime, stepTime, path ):
        
        event = threading.Event()
        runData = []
        
        for i in range(totalTime): 
            if i==0:
                self.lastData = self.car.getDrivingData()
                self.lastLocation = self.car.getVehiculeLocation()
            
            else:    
                 #setup current data
                self.currentData = self.car.getDrivingData()
                self.currentLocation = self.car.getVehiculeLocation()

                # Add stepData to runData And save picture
                runData.append(self.__collectSingleEnvironmentData())

                self.lastData = self.currentData
                self.lastLocation = self.currentLocation
            event.wait(stepTime)
        return runData


    def CollectPictures( self, totalTime, stepTime, path ):
        event = threading.Event()

        for i in range(totalTime): 
            self.__collectSinglePicture(path)
            event.wait(stepTime)