import csv
import os
from pathlib import Path
from datetime import date

# column order in "data"(Accelleration, Direction, brake, isCarInFront,DistanceToClosestCar, isPedestrianInFront, DistanceToClosestPedestrian, trafficLightInFront, distanceToTrafficLight, waypointDistance_x, waypointDistance_y ):
def ExportDataToCSVYaw(data):



    Headers =(["acceleration", 
                "direction" ,
                "brake" ,             
                "isCarInFront" ,
                "DistanceToClosestCar" ,
                "isPedestrianInFront" ,
                "distanceToClosestPedestrian" ,
                "trafficLightInFront" ,
                "distanceToTrafficLight" ,
                "deltaYaw"])



    __writeData(data, Headers, "All Data")


def __writeData(data, headers, filename):

    today = date.today()
    data_folder = Path("data/")
    file = filename+today.strftime(" %d-%m-%Y")+".csv"
    path = data_folder / file

    if os.path.exists(path):
        append_write = 'a' # append if already exists
    else:
        append_write = 'w' # make a new file if not

    with open(path, append_write, newline='') as csvFile:
        writer = csv.writer(csvFile)
        if (append_write == 'w'):
            writer.writerow(headers)
        for line in data:
            writer.writerow([
                line[0], #Accelerate / Direction
                line[1], #Direction
                line[2], #Brake
                line[3], #isCarInFront
                line[4], #DistanceToClosestCar
                line[5], #isPedestrianInFront
                line[6], #DistanceToClosestPedestrian
                line[7], #trafficLightInFront
                line[8], #distanceToTrafficLight
                line[9], #deltaYaw
               ])
