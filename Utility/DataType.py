# https://carla.readthedocs.io/en/latest/python_api/#carla.Image
PICTURE = "picture"

# https://carla.readthedocs.io/en/latest/python_api/#carlacollisionevent
COLLISION = "collision"

# https://carla.readthedocs.io/en/latest/python_api/#carlagnssmeasurement
GPS = "gps"