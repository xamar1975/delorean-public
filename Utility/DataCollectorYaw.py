import threading
import os
import config
import random
import numpy as np
from Utility import misc

from Utility import RecognitionType
from Utility import ExportToCSVYaw
from Car.Control.Concrete_implementation import CarlaCar
from Car.Perception.Concrete_implementation import ObjectRecognitionMOCK
from Car.Planning.Concrete_implementation.CarlaRoutePlanner import CarlaRoutePlanner
# necessary to use Carla.XXXXX
import sys
sys.path.append( config.PATH_TO_EGG )
import carla

class DataCollectorYaw():

    def __init__( self, client, carlaCar ): # CarlaDataCollector(totalTime, path, testCar, stepTime) 


        self.currentData = []
        self.currentLocation = None
        self.currentWaypoint = None
        
        self.endWaypoint = None

        self.redLightDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK(RecognitionType.TRAFFIC_LIGHT)
        self.carDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK(RecognitionType.CAR)
        self.pedestrianDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK(RecognitionType.PEDESTRIAN)

        
        self.car = carlaCar
        self.client = client
        self.mapp = client.get_world().get_map()
        self.routePlanner = CarlaRoutePlanner(self.client, self.car)

        self.event = threading.Event()


    # def CollectData( self, totalTime, stepTime, path ):
        
    #     runData = self.CollectEnrionmentData(totalTime, stepTime, path)
    #     self.CollectPictures(totalTime, stepTime, path)

    #     return runData


    def CollectData( self, totalTime, path ):
        runData = []  
        endWaypoint = self.routePlanner.getEndWaypoint()

        for i in range(totalTime):  
            #if car is at destination, create new destination  
            if self.routePlanner.isAtWaypoint(endWaypoint):
                randomWaypoint = self.routePlanner.getRandomWaypoint()
                self.routePlanner.setDestinationWaypoint(randomWaypoint)

            stepData = self.__collectSingleEnvironmentData(i)
            runData.append(stepData)
            #self.__collectSinglePicture(path)

            world_snapshot = self.client.get_world().wait_for_tick()
            #self.event.wait(config.STEP_TIME)

            if (i%100 == 0):
                ExportToCSVYaw.ExportDataToCSVYaw(runData)
                runData = []

        ExportToCSVYaw.ExportDataToCSVYaw(runData)

    def __collectSingleEnvironmentData(self, tickNumber):
        #setup current data
        self._setCurrentData()
        singleData = []


        acceleration = f"{self.currentData[0]:.8f}" 
        direction = f"{self.currentData[1]:.8f}"
        brake = f"{self.currentData[2]:.8f}"
        isCarInFront = (len(self.carDetector.detectObjects(self.car))>0)
        distanceToCar = "0.0"
        isPedestrianInFront = (len(self.pedestrianDetector.detectObjects(self.car))>0)
        distanceToPedestrian = "0.0"
        isTrafficLightInFront = (len(self.redLightDetector.detectObjects(self.car))>0)
        distanceToTrafficLight = "0.0"
        vehicleYaw =  self.car.carPointer.get_transform().rotation.yaw
        vehicleLocation = self.car.getVehiculeLocation()
        waypointYaw = self.car.getCurrentWaypoint().transform.rotation.yaw
        #nextWPLocation = self.routePlanner.getRoute()[self.routePlanner.getIndex()+1][0].transform.location
        deltaYaw = self.routePlanner.getAngle(vehicleYaw, waypointYaw)
        
        print("\n ------------"+str(tickNumber)+"------------------")
        print("Acceleration: " + str( acceleration ))
        print("Direction: " + str( direction ))
        print("brake: " + str( brake ))
        #print("Is a car in front: "+ str( isCarInFront ))
        #print("Distance to car in front: "+ distanceToCar )
        #print("is a pedestian in front: "+ str(  isPedestrianInFront ))
        #print("Distance to pedestian in front: "+ distanceToPedestrian )
        print("Is a traffic Light in front: "+ str(  isTrafficLightInFront ))
        #print("Distance to Light in front: "+ distanceToTrafficLight )
        print("Car Yaw      = "+ str(vehicleYaw))
        print("Waypoint yaw = "+ str(waypointYaw))
        #print("Car location     = " + str(vehicleLocation))
        #print("Next WP location = "+str(nextWPLocation))
        print("delta Yaw = "+ str(deltaYaw))

        singleData.append(acceleration) #accelleration
        singleData.append(direction) #DirectionDirection
        singleData.append(brake) #brake
        singleData.append(1 if isCarInFront else 0) #isCarInFront
        singleData.append(distanceToCar) #DistanceToClosestCar
        singleData.append(1 if isPedestrianInFront else 0) #isPedestrianInFront
        singleData.append(distanceToPedestrian) #DistanceToClosestPedestrian
        singleData.append(1 if isTrafficLightInFront else 0) #trafficLightInFront
        singleData.append(distanceToTrafficLight) #distanceToTrafficLight
        singleData.append(deltaYaw) #yaw difference

        return singleData


    def __collectSinglePicture(self, path):
        image = None

        for sensor in self.car.sensors:
            if sensor.role == 'front_car_image': 
                image = sensor.getData()

                if self.car.is_at_traffic_light() and self.car.get_traffic_light():
                    if not os.path.exists(f'{path}/red_light'):
                        os.mkdir(f'{path}/red_light')
                    image.save_to_disk(f'{path}/red_light/%.6d.jpg' % image.frame_number)
                else:
                    if not os.path.exists(f'{path}/not_red_light'):
                        os.mkdir(f'{path}/not_red_light')
                    image.save_to_disk(f'{path}/not_red_light/%.6d.jpg' % image.frame_number)

        
    def CollectEnvironmentData( self, totalTime, stepTime, path ):
        
        event = threading.Event()
        runData = []
        
        for i in range(totalTime): 
            if i==0:
                self.data = self.car.getDrivingData()
                self.location = self.car.getVehiculeLocation()
            
            else:    
                 #setup current data
                self.nextData = self.car.getDrivingData()
                self.nextLocation = self.car.getVehiculeLocation()

                # Add stepData to runData And save picture
                runData.append(self.__collectSingleEnvironmentData())


            event.wait(stepTime)
        return runData


    def CollectPictures( self, totalTime, stepTime, path ):
        event = threading.Event()

        for i in range(totalTime): 
            self.__collectSinglePicture(path)
            event.wait(stepTime)

    def _setCurrentData(self):
        self.currentData = self.car.getDrivingData()
        self.currentLocation = self.car.getVehiculeLocation()
        self.currentWaypoint = self.car.getCurrentWaypoint()



