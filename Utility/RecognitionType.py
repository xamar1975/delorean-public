
# header name
TRAFFIC_LIGHT = "trafficlight"
# possible values
TRAFFIC_LIGHT_NONE = "0"
TRAFFIC_LIGHT_GREEN = "1"
TRAFFIC_LIGHT_YELLOW = "2"
TRAFFIC_LIGHT_RED = "3"
TRAFFIC_LIGHT_NOT_RED = "4"

# header name
TRAFFIC_LIGHT_DISTANCE_X = "trafficlight-distance-x"
TRAFFIC_LIGHT_DISTANCE_Y = "trafficlight-distance-y"
# possible values are float
TRAFFIC_LIGHT_DISTANCE_NONE = "0.0"

# header name
CAR = "car"
# possible values
CAR_NONE = "0"
CAR_IN_FRONT = "1"

# header name
CAR_DISTANCE_X = "car-distance-x"
CAR_DISTANCE_Y = "car-distance-y"
# possible values are float
CAR_DISTANCE_NONE = "0.0"

# header name
PEDESTRIAN = "pedestrian"
# possible values
PEDESTRIAN_NONE = "0"
PEDESTRIAN_IN_FRONT = "1"

# header name
PEDESTRIAN_DISTANCE_X = "pedestrian-distance-x"
PEDESTRIAN_DISTANCE_Y = "pedestrian-distance-y"
# possible values are float
PEDESTRIAN_DISTANCE_NONE = "0.0"
