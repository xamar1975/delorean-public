
class Object():

    def __init__( self, name, distance ):

        self.name = name
        self.distance = distance

    def getName( self ):
        
        return self.name
    
    def getDistance( self ):
        
        return self.distance

    def setName( self, name ):
        
        self.name = name
    
    def setDistance( self, distance ):
        
        self.distance = distance
