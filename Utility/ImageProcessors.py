
import cv2
import numpy as np

import config

def processRGB( image ):

    i = np.array( image.raw_data )
    i2 = i.reshape( (config.SENSORS_IMAGE_HEIGHT, config.SENSORS_IMAGE_WIDTH, 4) )
    i3 = i2[:, :, :3]

    return i3/255.0

def trafficlightsImageProcess( image ):

    i = np.array( image.raw_data )
    i2 = np.reshape( i, (image.height, image.width, 4) )
    i3 = i2[:, :, :3]
    i3 = i3/255.0 # may be necessary ???
    i4 = np.expand_dims( i3, axis=0 )

    return i4