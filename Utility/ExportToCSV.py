import csv
import os
from pathlib import Path
from datetime import date

# column order in "data"(accelleration, steerDirection, isCarInFront,DistanceToClosestCar, isPedestrianInFront, DistanceToClosestPedestrian, trafficLightInFront, distanceToTrafficLight, waypointDistance_x, waypointDistance_y ):
def ExportDataToCSV(data):
    accelerationHeaders =(["Acceleration", 
                           "isCarInFront" ,
                           "DistanceToClosestCar" ,
                           "isPedestrianInFront" ,
                           "DistanceToClosestPedestrian" ,
                           "trafficLightInFront" ,
                           "distanceToTrafficLight" ,
                           "waypointDistance_x" ,
                           "waypointDistance_y"])

    steerHeaders =(["Direction", 
                    "isCarInFront" ,
                    "DistanceToClosestCar" ,
                    "isPedestrianInFront" ,
                    "DistanceToClosestPedestrian" ,
                    "trafficLightInFront" ,
                    "distanceToTrafficLight" ,
                    "waypointDistance_x" ,
                    "waypointDistance_y"])

    __writeData(data, accelerationHeaders, "Acceleration Data")
    __writeData(data, steerHeaders, "Direction Data")

def __writeData(data, headers, filename):

    today = date.today()
    data_folder = Path("data/")
    file = filename+today.strftime(" %d-%m-%Y")+".csv"
    path = data_folder / file
    if filename == "Acceleration Data":
        accelerateOrSteer = 0
    elif filename == "Direction Data":
        accelerateOrSteer = 1

    if os.path.exists(path):
        append_write = 'a' # append if already exists
    else:
        append_write = 'w' # make a new file if not

    with open(path, append_write, newline='') as csvFile:
        writer = csv.writer(csvFile)
        if (append_write == 'w'):
            writer.writerow(headers)
        for line in data:
            writer.writerow([
                line[accelerateOrSteer], #Accelerate / Direction
                line[2], #isCarInFront
                line[3], #DistanceToClosestCar
                line[4], #isPedestrianInFront
                line[5], #DistanceToClosestPedestrian
                line[6], #trafficLightInFront
                line[7], #distanceToTrafficLight
                line[8], #waypointDistance_x
                line[9] #waypointDistance_y
               ])