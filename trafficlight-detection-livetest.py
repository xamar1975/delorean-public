
print( "\n--Loading libraries :\n" )

import time
import config
#import cv2
import numpy as np
import threading
import sys

from scipy import misc, ndimage

import tensorflow as tf
from keras_preprocessing import image
from keras_preprocessing.image import ImageDataGenerator
#from keras.models import load_model

sys.path.append( config.PATH_TO_EGG )
import carla

print( "All libraries loaded successfully !\n" )

print("\n-- Version Check :\n")

print( "Tensorflow version : " + tf.__version__ )

print("\n-- GPU Check :\n")

device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
    print('GPU device not found')
else:
	print( 'Found GPU at: {}'.format(device_name) )

print("\n-- Model Load :\n")

loaded_model = tf.keras.models.load_model( config.PATH_TO_TRAFFICLIGHT_MODEL )

print("\n-- Model Summary :\n")

loaded_model.summary()

print( "--Loading Classes :\n" )

from Car.Control.Concrete_implementation import CarlaCar
from Car.Sensors.Concrete_implementation import CarlaSensor
from Environments.Concrete_implementation.CarlaEnvironmentControler import CarlaEnvironmentControler
from Environments.Concrete_implementation.carlaClientConnector import CarlaClientConnector
from Utility import ImageProcessors
from Utility import DataType

print( "All classes loaded successfully !\n" )

def labelDisplay( probability ):

    label = ''

    if probability < 0.5 :
        label = 'Nored'
    else:
        label = 'Red'
	
    print(label)

def isTrafficLightRedInFront( probability ):

    result = True

    if probability < 0.5 :
        result = False
	
    return result

print( "--Starting client connection :\n" )

try:
    connector = CarlaClientConnector()
    
    if connector.testServerAvailability():

        client = connector.getClientConnection()
        event = threading.Event()

        print( "--Adding actors and main self-driven car :\n" )

        #spawnPoint = 1
        #spawnPoint = 50 #front of the tunnel
        spawnPoint = 40
        testCar = CarlaCar.CarlaCar()

        testCar.createCarInClient( client, config.CAR_MODEL, spawnPoint )

        for sensor in config.SENSORS:
            testCar.addSensor(
                sensor['TYPE'],
                sensor['X_POS'],
                sensor['Z_POS'],
                sensor['DATA_TYPE'], 
                sensor['ROLE'] )

        rgbSensor = testCar.sensors[0]
        
        print( "--Testing car controls :\n" )

        testCar.startAutoPilot()
        event.wait(2)        
        
        for i in range( 100 ):

            #data = testCar.getDrivingData()

            #if len(data) > 0:
            #    acceleration = str( round( data[0], 3) )
            #    steer = str( round( data[1], 3) )
            #    print(' Acceleration : ' + acceleration )
            #    print(' Steer : ' + steer + '\n' )
            
            picture = rgbSensor.getData()
            
            data = ImageProcessors.trafficlightsImageProcess( picture )
            
            result = loaded_model.predict( data )
            prediction = result[0][0]

            print(' Frame #' + str(i) )
            labelDisplay( prediction )
            print(prediction)

            #fileName = str(i) + ' ' + label
            #picture.save_to_disk( 'Sensors_data/' + fileName + '.jpg' )

            event.wait(2)

finally:

    #### remove actors from client and close connection ####
    testCar.removeCarInClient()
    #envControl.removeEverything()
    connector.terminateConnection()

    print( "Fin.\n" )
