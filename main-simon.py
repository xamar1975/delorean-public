print("\n--Loading libraries :\n")

import threading

import carla

import config
from Planning import global_route_planner
from Planning import global_route_planner_dao
from Utility import DataCollector

print("All libraries loaded successfully !\n")

print("--Loading Classes :\n")

from Car.Control.Concrete_implementation import CarlaCar
from Environments.Concrete_implementation.carlaClientConnector import CarlaClientConnector

print("All classes loaded successfully !\n")

print("--Starting client connection :\n")

try:
    connector = CarlaClientConnector()

    if connector.testServerAvailability():

        client = connector.getClientConnection()
        event = threading.Event()

        print("--Adding actors and main self-driven car :\n")

        mapp = client.get_world().get_map()
        spawnPoint = 1
        # 1 = Location(x=65.517593, y=7.808437, z=-0.006215)
        # 40 = Location(x=-9.441484, y=87.495155, z=-0.005568)

        locA = carla.Location(x=65.517593, y=7.808437, z=-0.006215)
        locB = carla.Location(x=-9.441484, y=87.495155, z=-0.005568)

        # spawnPoint = 50 #front of the tunnel
        testCar = CarlaCar.CarlaCar()

        testCar.createCarInClient(client, config.CAR_MODEL, 40)  # spawnPoint

        # to add car's sensors
        for sensor in config.SENSORS:
            testCar.addSensor(
                sensor['TYPE'],
                sensor['X_POS'],
                sensor['Z_POS'],
                sensor['DATA_TYPE'],
                sensor['ROLE'])

        event.wait(2)

        waypointB = mapp.get_waypoint(testCar.carPointer.get_location())
        testCar.removeCarInClient()
        testCar.createCarInClient(client, config.CAR_MODEL, 1)  # spawnPoint )
        waypointA = mapp.get_waypoint(testCar.carPointer.get_location())

        print("n\ n\ -------------------------------------------")
        print("waypoint A = " + str(waypointA))
        # print (waypointA)
        print("n\ n\ -------------------------------------------")
        print("waypoint B = " + str(waypointB))
        # print (str(waypointB))

        print("--Testing car controls :\n")

        # testCar.startAutoPilot()
        event.wait(2)
        plannerDAO = global_route_planner_dao.GlobalRoutePlannerDAO(mapp, 1)
        planner = global_route_planner.GlobalRoutePlanner(plannerDAO)
        planner.setup()
        event.wait(2)
        # waypoints = planner.trace_route(locA, locB)

        carDataCollector = DataCollector.DataCollector(testCar)
        path = "data/"
        runData = carDataCollector.CollectData(100, 1, path)


finally:
    # ExportToCSV.ExportDataToCSV(runData) if runData else None
    #### remove actors from client and close connection ####
    testCar.removeCarInClient()
    # envControl.removeEverything()
    connector.terminateConnection()

    print("Fin.\n")
