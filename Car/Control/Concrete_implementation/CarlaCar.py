#libraries
import os
import sys
import random
import math
import time

import config #config file

from ..Abstraction import I_Car

from Car.Sensors.Concrete_implementation import CarlaSensor

import carla

class CarlaCar( I_Car.ICar ):

    def __init__( self ):  
        
        """[ Initialise the private parameters of the object to their default values ]
        """
        self.carIsLive = False
        self.lastKnownLocation = None

        # those values are used for manual car control
        self.currentThrottle = 0.0
        self.currentBrake = 0.0
        self.currentSteer = 0.0
        ##############################################

        self.carPointer = None
        self.sensors = []
        self.isAutoPilotOn = False

    @property
    def id(self):
        return self.carPointer.id

    def applyDrivingDecisions( self, control ):
        """[ Method used by the agent class to send new control parameters to the Carla.actor which is the actual vehicule ]
        """
        self.carPointer.apply_control( control )    

    def __applyVehicleControl( self ):
        """[ Private method used by other control actions to send new control parameters to the Carla.actor which is the actual vehicule ]
        """
        controls = carla.VehicleControl( throttle= self.currentThrottle, brake= self.currentBrake, steer= self.currentSteer )        
        self.carPointer.apply_control( controls )

    def modifyAcceleration( self, value ):
        """[ Is used to initiate a change in actor behavior to accelarate or deccelarate the vehicule using a % value as a float ]

        Args:
            value ([ float ]) : [ Float value of the new throttle or brake value for the manual controls, 
                                 goes from -1.0 to 1.0, 1.0 represent full throttle and -1.0 represent full brake ]
        """

        # to limit value to 1.0 which is 100% acceleration
        if value > 1.0:
            value = 1.0

        # to limit value to -1.0 which is 100% brake
        if value < -1.0:
            value = -1.0

        if value < 0.0:
            self.__accelerate( value )
        else:
            value = value * -1
            self.__brake( value )

    def __brake( self, value ):
        """[ Is used to initiate a change in actor behavior to deccelarate the vehicule using a % value as a float ]

        Args:
            value ([ float ]) : [ Float value of the new brake value for the manual controls, 
                                 goes from 0.0 to 1.0, 1.0 represent full brake]
        """
        #preconditions
        if value > 0.0 and self.carIsLive:
            
            # to limit value to 1.0 which is 100% brake
            if value > 1.0:
                value = 1.0
            
            self.currentThrottle = 0.0
            self.currentBrake = value

            self.__applyVehicleControl()

    def __accelerate( self, value ):
        """[ Is used to initiate a change in actor behavior to accelarate the vehicule using a % value as a float ]

        Args:
            value ([ float ]) : [ Float value of the new throttle value for the manual controls, 
                                 goes from 0.0 to 1.0, 1.0 represent full throttle ]
        """

        #preconditions
        if value > 0.0 and self.carIsLive:
            
            # to limit value to 1.0 which is 100% throttle
            if value > 1.0:
                value = 1.0            

            self.currentThrottle = value
            self.currentBrake = 0.0

            self.__applyVehicleControl()

    def turnCar( self, value ):
        """[ Is used to initiate a change in actor behavior to turn the vehicule using a % value as a float
            negatives values represent the vehicule turning left,
            positives values represent the vehicule turning right
            and 0.0 represent the vehicule going straight ]

        Args:
            value ([ float ]) : [ Float value of the new steer value for the manual controls, 
                                 goes from -1.0 to 1.0, 1.0 represent full right and -1.0 represent full left ]
        """
        #preconditions
        if self.carIsLive:

            # to limit positives value to 1.0 which is 100% right
            if value > 1.0:
                value = 1.0

            # to limit negatives value to -1.0 which is 100% left
            if value < -1.0:
                value = -1.0

            self.currentSteer = value

            self.__applyVehicleControl()
     
    def getSpeed( self ):
        """[ Is used to retrieve the speed of the vehicule ]

        Returns:
            [ Integer ]: [ the speed of the vehicule actor in KM/H ]
        """

        velocity = self.carPointer.get_velocity()
        actualSpeed = int(3.6 * math.sqrt( velocity.x**2 + velocity.y**2 + velocity.z**2))
        return actualSpeed 
        
    def addSensor( self, sensorType, X_Pos, Z_Pos, dataType, role ):
        """[ Used to create and add a new sensor to the actual Carla.actor vehicule ]

        Args:
            sensorType ([ string ]): [ name of the blueprint for the new sensor ]
            X_Pos ([ float ]): [ x position for the sensor ]
            Z_Pos ([ float ]): [ z position for the sensor ]
            dataType ([ string ]): [ type of data that the sensor collect ]
            role ([ string ]): [ role of the sensor in the car ]

        Returns:
            [ Carla.Sensor ]: [ return a reference to the actual sensor in the Carla environnement ]
        """        

        world = self.client.get_world()
        blueprint_library = world.get_blueprint_library()
        blueprint = blueprint_library.find( sensorType )        
        
        #TODO add code for all type of sensors

        if sensorType != 'sensor.other.gnss':
            blueprint.set_attribute( 'image_size_x', f'{ config.SENSORS_IMAGE_WIDTH }' )
            blueprint.set_attribute( 'image_size_y', f'{ config.SENSORS_IMAGE_HEIGHT }' )
            blueprint.set_attribute( 'fov', '110' )

        spawn_point = carla.Transform( carla.Location( x=X_Pos, z=Z_Pos ) )

        newSensor = world.spawn_actor( blueprint, spawn_point, attach_to=self.carPointer )

        carlaSensor = CarlaSensor.CarlaSensor( dataType, self.client, newSensor, role )
        carlaSensor.sensorIsLive = True

        self.sensors.append( carlaSensor )
        
        print( "Sensor was added successfully !\n" )
        
        return carlaSensor

    def createCarInClient( self, client, carModel, spawnPoint ):        
        """[ Used after the client connection to instanciate the car in the carla environnement,
            this method is a shell ; it verify every parameters and then make several spawning attemps using
            the private method __initiateCar( client, carModel, spawnPoint )]

        Args:
            client ( Carla.client ): [ connection to the carla client using carla.Client( HOST_ADDRESS, PORT ) ]
            carModel ( string ): [ string that represent the model of the desired car ex.) "model3" ]
            spawnPoint ( integer ): [ integer that represent the number of the desired spawnpoint
                                      in the array received from world.get_map().get_spawn_points() ]

        Returns:
            [ boolean ] : [ represent the validity of the parameters and the success of the car creation ]
        """
        errors = False

        if client is None:
            errors = True
            print("Error with the parameter : client\n")

        if carModel == "":
            errors = True
            print("Error with the parameter : carModel\n")

        if spawnPoint < 0 or spawnPoint > 255:
            errors = True
            print("Error with the parameter : spawnPoint\n")

        if not errors:
            isCarInitiatedSuccessfully = self.__initiateCar( client, carModel, spawnPoint )
            attemps = 1
            attempsMaximum = 5
            waitTime = 2

            while not isCarInitiatedSuccessfully and attemps <= attempsMaximum:
                time.sleep( waitTime )
                isCarInitiatedSuccessfully = self.__initiateCar( client, carModel, spawnPoint )
                attemps += 1

            if isCarInitiatedSuccessfully:
                print("Car was created successfully on attemp #" +  str(attemps) + " !\n")
            else:
                print("All " +  str(attempsMaximum) + " attemps failed.\n")   
                errors = True

        return not errors

    def __initiateCar( self, client, carModel, spawnPoint ):        
        """[ Is used to attemp to create and spawn the car in the Carla environnement ]

        Args:
            client ( Carla.client ): [ connection to the carla client using carla.Client( HOST_ADDRESS, PORT ) ]
            carModel ( string ): [ string that represent the model of the desired car ex.) "model3" ]
            spawnPoint ( integer ): [ integer that represent the number of the desired spawnpoint
                                      in the array received from world.get_map().get_spawn_points() ]

        Returns:
            [ boolean ] : [ represent the success of the car creation ]
        """
        isSuccessfullSpawnAttemp = False

        try:
            self.client = client

            world = self.client.get_world()
            blueprint_library = world.get_blueprint_library()

            bluePrint = blueprint_library.filter( carModel )[0]

            spawn_point = world.get_map().get_spawn_points()[ spawnPoint ]

            vehicle = world.spawn_actor( bluePrint, spawn_point )
                
            self.carIsLive = True
            self.carPointer = vehicle
            light_state = carla.VehicleLightState.LowBeam
            #light_state = carla.VehicleLightState.HighBeam
            self.carPointer.set_light_state( light_state )            

            isSuccessfullSpawnAttemp = True
        
        finally:
            return isSuccessfullSpawnAttemp

    def removeCarInClient( self ):
        """[ Will remove the car and its sensors(if any) from the Carla environnement ]

        Returns:
            [ boolean ] : [ represent the success of the operation ]
        """

        # pre-conditions
        if self.carIsLive:

            self.carIsLive = False

            for sensor in self.sensors:
                sensor.removeSensorInClient()

            self.client.apply_batch( [carla.command.DestroyActor(self.carPointer)] ) 
            self.carPointer = None

            print("Car was removed successfully !")
            print("All car sensors were removed successfully !\n")
            return True
        else:
            print("Car could not be removed from client.\n")
            return False

    def startAutoPilot( self ):
        """[ Is used to initiate a change in actor behavior to activate the build-in Carla autopilot ]
        """
        # pre-conditions
        if self.carIsLive and not self.isAutoPilotOn :
            self.isAutoPilotOn = True
            self.carPointer.set_autopilot( True )
            print("Car auto-pilot is on !\n")

    def stopAutoPilot( self ):
        """[ Is used to initiate a change in actor behavior to deactivate the build-in Carla autopilot ]
        """
        # pre-conditions
        if self.carIsLive and self.isAutoPilotOn :
            self.isAutoPilotOn = False
            self.carPointer.set_autopilot( True )
            print("Car auto-pilot is off !\n")

    def getDrivingData( self ):
        """[ Is used to retrieve vehicule data from the actual vehicule actor in Carla ]

        Returns:
            [ Float[2] ] : [ array that contains 2 float values ;
                            [0]accelaration, [1]steer ]
        """
        data = []

        # pre-conditions
        if self.carIsLive :

            control = self.carPointer.get_control()
            
            data.append( control.throttle )
            data.append( control.steer)
            data.append( control.brake )            
        
        return data
    
    def getVehiculeLocation( self ):
        """[ Is used to retrieve the actual position of the vehicule according to the Carla.Map ]

        Returns:
            [ Carla.Location ]: [ current location of the vehicule ]
        """
        location = None

        # pre-conditions
        if self.carIsLive :
            location = self.carPointer.get_location()

        return location

    def get_traffic_light(self):
        return self.carPointer.get_traffic_light()

    def is_at_traffic_light(self):
        return self.carPointer.is_at_traffic_light()

    def getCurrentWaypoint(self):
        return  self.client.get_world().get_map().get_waypoint(self.getVehiculeLocation(), project_to_road=True)

    def getTransform(self):
        return self.carPointer.get_transform()


            
