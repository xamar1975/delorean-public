# abc == Abstract base class
from abc import ABC, abstractclassmethod

class ICar( ABC ): 
    """[ Used by concrete impementations to control the vehicule in the simulator,
        represent every methods used to control the car.]

    """
    @abstractclassmethod
    def modifyAcceleration( self, value ):
        pass

    @abstractclassmethod
    def turnCar( self, value ):
        pass

    @abstractclassmethod
    def getSpeed( self ):
        pass

    @abstractclassmethod
    def addSensor( self, sensor ):
        pass

    @abstractclassmethod
    def startAutoPilot( self ):
        pass

    @abstractclassmethod
    def stopAutoPilot( self ):
        pass

    @abstractclassmethod
    def getDrivingData( self ):
        pass

    @abstractclassmethod
    def getVehiculeLocation( self ):
        pass