
from ..Abstraction import I_ObjectDetector

class ObjectRecognition( I_ObjectDetector.IObjectDetector ):

    def __init__( self, recognitionType ):  

        self.modelPath = ""
        self.model = None
        self.sensor = None
        self.recognitionType = recognitionType

    def loadModel( self, path ):

        if len( path ) > 0:

            self.modelPath = path
            
            ###################################
            ## TO-DO : Model loading logic
            ###################################

            print("Model was loaded successfully !\n")

    def addSensor( self, sensor ):
        self.sensor = sensor

    def detectObjects( self ):

        objects = []

        ###################################
        ## TO-DO : Object detection logic
        ###################################

        return objects

    def getRecognitionType( self ):
        
        return self.recognitionType