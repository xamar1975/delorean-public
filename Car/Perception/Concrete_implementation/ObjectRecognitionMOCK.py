
from Car.Control.Concrete_implementation import CarlaCar
from Utility import RecognitionType
from ..Abstraction import I_ObjectDetector

class ObjectRecognitionMOCK( I_ObjectDetector.IObjectDetector ):

    def __init__( self, recognitionType ):  

        self.modelPath = ""
        self.model = None
        self.sensor = None
        self.recognitionType = recognitionType

    def loadModel( self, path ):

        if len( path ) > 0:

            self.modelPath = path
            print("Model was loaded successfully !\n")

    def addSensor( self, sensor ):
        
        self.sensor = sensor

    def detectObjects( self, carlaCar ):
        objects = []
        vehicle = carlaCar.carPointer
        if ((self.recognitionType == RecognitionType.TRAFFIC_LIGHT) and (vehicle.is_at_traffic_light())):
            tl= vehicle.get_traffic_light()
            objects.append(tl)
            
        return objects
    
    def getRecognitionType( self ):
        
        return self.recognitionType