
from ..Abstraction import I_ObjectDetector
from Utility import ImageProcessors
from Utility import Object

import tensorflow as tf

class TrafficLightRecognition( I_ObjectDetector.IObjectDetector ):

    def __init__( self, recognitionType ):  

        self.model = None
        self.isModelLoaded = False

        self.sensor = None
        self.isSensorAdded = False
        
        self.recognitionType = recognitionType

    def loadModel( self, path ):

        if len( path ) > 0:

            self.model = tf.keras.models.load_model( path )
            self.isModelLoaded = True

            print("Model was loaded successfully !\n")

    def addSensor( self, sensor ):

        if sensor is not None :

            self.sensor = sensor
            self.isSensorAdded = True

            print("Sensor was added succesfully to the detector !\n")

    def detectObjects( self ):

        objects = []

        if self.isModelLoaded and self.isSensorAdded :

            picture = self.sensor.getData()
            
            #data formating
            data = ImageProcessors.trafficlightsImageProcess( picture )
                
            result = self.model.predict( data )
            prediction = result[0][0]

            if self._isTrafficLightRedInFront(prediction):

                newObject = Object.Object( "RedLight", 1.0 )
                objects.append( newObject )        

        return objects

    def detectObjectsDebug( self ):

        objects = []

        if self.isModelLoaded and self.isSensorAdded :

            picture = self.sensor.getData()
            
            #data formating
            data = ImageProcessors.trafficlightsImageProcess( picture )
                
            result = self.model.predict( data )
            prediction = result[0][0]

            ### only for DEBUG ###
            self._labelDisplay( prediction )

            if self._isTrafficLightRedInFront(prediction):

                newObject = Object.Object( "RedLight", 1.0 )
                objects.append( newObject )        

        return objects

    def _labelDisplay( probability ):

        label = ''

        if probability < 0.5 :
            label = 'Nored'
        else:
            label = 'Red'
	
        print(label)

    def getRecognitionType( self ):
        
        return self.recognitionType

    def _isTrafficLightRedInFront( probability ):

        result = True

        if probability < 0.5 :
            result = False
	
        return result