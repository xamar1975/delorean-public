# abc == Abstract base class
from abc import ABC, abstractclassmethod

class IObjectDetector( ABC ): 

    @abstractclassmethod
    def loadModel( self, path ):
        pass

    @abstractclassmethod
    def addSensor( self, sensor ):
        pass

    @abstractclassmethod
    def detectObjects( self ):
        pass

    @abstractclassmethod
    def getRecognitionType( self ):
        pass
