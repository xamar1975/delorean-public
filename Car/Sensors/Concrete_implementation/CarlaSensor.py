from ..Abstraction import ISensor
from Utility import ImageProcessors

import config
import sys
sys.path.append( config.PATH_TO_EGG )

import carla
import time

class CarlaSensor( ISensor.ISensor ):
    """[ Concrete implementation of the ISensor Interface,
         is used to encapsulate the sensor from the Carla Simulator ]

    Args:
        ISensor ([Interface]): [ Contain the prototype of the getData and the getDataType methods ]
    """
    def __init__( self, dataType, carlaClient, carlaSensorPointer, role  ):  
        
        self.sensorPointer = carlaSensorPointer
        self.sensorDataType = dataType
        self.lastData = None
        self.client = carlaClient
        self.sensorIsLive = False 
        self.role = role

    def __processData( self, data ):
        """[ Private method, is used to temporary store the data from the sensor ]
        """
        self.lastData = data
    
    def getData( self ):
        """[ Is used to get data from the simulation,
             data format will depend from the dataTypes stored in DeepDriver/Utility/DataType.py ]

        Returns:
            [ ]: [ depends from the DataType of the sensor ]
        """
        data = None

        #preconditions
        if self.sensorPointer is not None :

            self.sensorPointer.listen( lambda data: self.__processData(data) )

            while self.lastData is None:
                time.sleep(0.01)
            
            self.sensorPointer.stop()
            data = self.lastData
            self.lastData = None

        return data

    def getDataType( self ):
        """[ Is used to get the DataType of the sensor ]

        Returns:
            [ String ]: [ DataType from DeepDriver/Utility/DataType.py ]
        """
        return self.sensorDataType
    
    def removeSensorInClient( self ):
        """[ Will remove the car and its sensors(if any) from the Carla environnement ]

        Returns:
            [ boolean ] : [ represent the success of the operation ]
        """

        # pre-conditions
        if not self.sensorIsLive:
            print("Sensor could not be removed from client.\n")
            return False

        self.sensorIsLive = False
        self.client.apply_batch( [carla.command.DestroyActor(self.sensorPointer)] ) 
        self.sensorPointer = None

        print("Sensor was removed successfully !")
        print("All car sensors were removed successfully !\n")
        return True