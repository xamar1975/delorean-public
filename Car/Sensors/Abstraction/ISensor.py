# abc == Abstract base class
from abc import ABC, abstractclassmethod

class ISensor( ABC ):
    """[ Used by concrete implementations to get data from the simulation ]
         represent every methods used to retrieve data ]
    """
    @abstractclassmethod
    def getData( self ):
        pass

    @abstractclassmethod
    def getDataType( self ):
        pass