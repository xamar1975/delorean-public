#libraries
import os
import sys
import random
import threading

from ..Abstraction import I_RoutePlanner
import config #config file
from Car.Planning.Concrete_implementation import global_route_planner
from Car.Planning.Concrete_implementation import global_route_planner_dao
from Car.Control.Concrete_implementation.CarlaCar import CarlaCar

import sys
sys.path.append( config.PATH_TO_EGG )
import carla

class CarlaRoutePlanner( I_RoutePlanner.I_RoutePlanner ):
 
    def __init__( self, carlaClient, carlaCar):
        self.event = threading.Event()
        self.client = carlaClient
        self.world = self.client.get_world()
        self.carlaCar = carlaCar


        self._grp = None
        self.currentWaypoint = None
        self.nextWaypoint = None
        self.expectedIndex = 0
        self.destinationWaypoint = self.getRandomWaypoint()
        self._grp = None


    def setDestinationWaypoint( self, destinationWaypoint):
        self.currentWaypoint = self.world.get_map().get_waypoint(self.carlaCar.getVehiculeLocation())
        self.endWaypoint = destinationWaypoint
        self.route = self._trace_route(self.currentWaypoint, self.endWaypoint)
        self.expectedIndex = 0


    def updateRouteIndex( self ):
        self.currentWaypoint = self.carlaCar.getCurrentWaypoint()
        indexFound = False

        for i in range (self.expectedIndex, len(self.route)):
            if not indexFound:
                indexFound = self.isAtWaypoint(self.route[i][0])
                
                if indexFound:
                    self.expectedIndex = i+1
                
                    self.world.debug.draw_string(self.route[i+1][0].transform.location, 'o', draw_shadow=False,
                        color=carla.Color(r=255, g=0, b=0), life_time=1.5,
                        persistent_lines=True)
        

    def getRoute( self ):
        return self.route

    def getIndex(self):
        self.updateRouteIndex()
        return self.expectedIndex

    def getAngle(self, vehicleYaw, waypointYaw):
        deltaYaw = waypointYaw-vehicleYaw
        
        if deltaYaw >300:
            deltaYaw -= 360
        elif deltaYaw >140:
            deltaYaw -=180
        
        if deltaYaw < -300:
            deltaYaw +=360
        elif deltaYaw <-140:
            deltaYaw +=180

        return deltaYaw


    def getRouteLeft(self):
        pass

    def getNextPoints(self, numberOfPoints):
        nextWaypoints = []
        for i in range(numberOfPoints):
            if self.route[i] != None:
                nextWaypoints.append(self.route[i][0])
        return nextWaypoints


    def _trace_route(self, start_waypoint, end_waypoint):
        """
        This method sets up a global router and returns the
        optimal route from start_waypoint to end_waypoint.

            :param start_waypoint: initial position
            :param end_waypoint: final position
        """
        # Setting up global router
        if self._grp is None:
            dao = global_route_planner_dao.GlobalRoutePlannerDAO(
                self.world.get_map(), sampling_resolution=config.NEXT_WAYPOINT_DISTANCE)
            grp = global_route_planner.GlobalRoutePlanner(dao)
            grp.setup()
            self._grp = grp

        # Obtain route plan
        print("end waypoint = "+ str(end_waypoint))
        route = self._grp.trace_route(
            start_waypoint.transform.location,
            end_waypoint.transform.location)

        return route
    def getEndWaypoint(self):
        return self.endWaypoint

    def getRandomWaypoint(self):
        randomWaypoint = None
        spawnTransforms = self.world.get_map().get_spawn_points()
        randomTransform = random.choice(spawnTransforms)
        randomWaypoint= self.world.get_map().get_waypoint(randomTransform.location)
        return randomWaypoint


    def isAtDestination(self):
        return self.isAtWaypoint(self.endWaypoint)


    def isAtWaypoint(self, waypoint):
        atWaypoint = False

        vehicleLocation = self.carlaCar.getVehiculeLocation() 
        xValues = [vehicleLocation.x, waypoint.transform.location.x]
        xValues.sort()
        yValues = [vehicleLocation.y, waypoint.transform.location.y]
        yValues.sort()

        deltaX= xValues[1]-xValues[0]
        deltaY= yValues[1]-yValues[0]
        if ((deltaX < 5) and (deltaY < 5)):
            atWaypoint = True
        return atWaypoint