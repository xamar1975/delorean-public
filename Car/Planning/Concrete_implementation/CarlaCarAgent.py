
#libraries
import os
import sys
import random
import math
import time
import threading

import config #config file
from Car.Planning.Concrete_implementation import CarlaRoutePlanner
import tensorflow as tf

from ..Abstraction import Agent
from ...Control.Concrete_implementation.CarlaCar import CarlaCar
from Utility import RecognitionType

#from Car.Perception.Concrete_implementation import ObjectRecognitionMOCK
from Car.Perception.Concrete_implementation import TrafficLightRecognition

# necessary to use Carla.XXXXX
import sys
sys.path.append( config.PATH_TO_EGG )
import carla

class CarlaCarAgent( Agent.Agent ):

    def __init__( self, carlaClient):
        #set start location 
        #set end location

        self.event = threading.Event()
        self.client = carlaClient
        self.world = self.client.get_world()
    
        # creating and spawning Car in client
        self.carlaCar = CarlaCar()
        self.spawnNumber = None
        self.currentWaypoint = None
        self.nextWaypoint = None
        self.routePlanner = CarlaRoutePlanner.CarlaRoutePlanner(self.client, self.carlaCar)
        
        # loading decision model
        self.decisionModel = tf.keras.models.load_model( config.PATH_TO_DECISION_MODEL )

        #using the model for the first time is slow, so we use it here then get rid of the result
        mockData = [[0 , 0.0 , 0 , 0.0 , 1 , 0 , 2.5]]
        results = self.decisionModel.predict( mockData )
        results = None

    def removeAgent( self ):
        return self.carlaCar.removeCarInClient()


    def drive(self, spawnNumber, destination):
        
        self.carlaCar.createCarInClient( self.client, config.CAR_MODEL, spawnNumber)
        # creating and spawning Sensors
        for sensor in config.SENSORS:
            self.carlaCar.addSensor(
                sensor['TYPE'],
                sensor['X_POS'],
                sensor['Z_POS'],
                sensor['DATA_TYPE'], 
                sensor['ROLE'] )

        rgbSensor = self.carlaCar.sensors[0]        

        # traffic-light detector
        self.redLightDetector = TrafficLightRecognition.TrafficLightRecognition( RecognitionType.TRAFFIC_LIGHT )
        self.redLightDetector.loadModel( config.PATH_TO_TRAFFICLIGHT_MODEL )
        self.redLightDetector.addSensor( rgbSensor )
        #self.redLightDetector = ObjectRecognitionMOCK.ObjectRecognitionMOCK( RecognitionType.TRAFFIC_LIGHT )        
        
        self.routePlanner.setDestinationWaypoint(destination)

        self.currentWaypoint = self.carlaCar.getCurrentWaypoint()   

                #TO-DO create function 
        while (not self.routePlanner.isAtDestination()):
            
            self.currentWaypoint = self.carlaCar.getCurrentWaypoint()
            nextWaypoints = []
            route = self.routePlanner.getRoute()
            index = self.routePlanner.getIndex()
            for i in range(index, index+7):
                nextWaypoints .append(route[i][0])

            for waypoint in nextWaypoints:
                self.world.debug.draw_string(waypoint.transform.location, 'o', draw_shadow=False,
                                       color=carla.Color(r=0, g=255, b=0), life_time=2.1,
                                       persistent_lines=True)

            control = self.run_step()
            self.carlaCar.applyDrivingDecisions(control)
            self.world.wait_for_tick()

        # for i in range(driveTime):
        #     currentWaypoint = self.world.get_map().get_waypoint(self.carlaCar.getVehiculeLocation())
        #     nextWaypoints =  currentWaypoint.next(config.NEXT_WAYPOINT_DISTANCE)
        #     control = self.run_step(nextWaypoints[0])
        #     self.carlaCar.applyDrivingDecisions(control)
        #     self.event.wait(1)


    def run_step( self):
        
        controls = self.__getDrivingDecision()
        
        return controls


    def __detectObstacle(self):

        # To-DO implement detection logic

        return False

    def __detectRedTrafficLight(self):

        #detectorResults = self.redLightDetector.detectObjects(self.carlaCar)
        # detectorResults = self.redLightDetector.detectObjects()
        detectorResults = self.redLightDetector.detectObjectsDebug()
        isTrafficLightInFront = len(detectorResults) > 0

        return isTrafficLightInFront


    def __getDrivingDecision(self):
        """[summary]

        Returns:
            [vehicleControl]: [a vehicleControl object to be used by]
        """
        isObstacleInFront = self.__detectObstacle()
        isRedTrafficInFront = self.__detectRedTrafficLight()
        
        #convert boolean response to 0 or 1
        obstacle = 1
        obstacleDistance = 0.0
        trafficLight = 1
        trafficLightDistance = 0.0
        
        if not isObstacleInFront :
            obstacle = 0

        if not isRedTrafficInFront:
            trafficLight = 0


        #print("\n\n---------------------------------------------")
        #print("next waypoint=  "+ str(self.nextWaypoint))

        self.routePlanner.updateRouteIndex()
        vehicleYaw = self.carlaCar.getTransform().rotation.yaw
        nextYaw = self.routePlanner.getRoute()[self.routePlanner.getIndex()][0].transform.rotation.yaw

        deltaYaw = self.routePlanner.getAngle(vehicleYaw, nextYaw)

        # used with distanceX and distanceY
        #stepData = [[obstacle,obstacleDistance,obstacle,obstacleDistance,trafficLight,trafficLightDistance,distanceX,distanceY]]
        stepData = [[obstacle,obstacleDistance,obstacle,obstacleDistance,trafficLight,trafficLightDistance,deltaYaw]]
        results = self.decisionModel.predict( stepData )
        print(results)

        stepAcceleration = results[0][0]
        stepSteer = results[0][1]
        stepBrake = results[0][2]

        decisionThrottle = stepAcceleration
        decisionBrake = stepBrake if stepBrake > 0.1 else 0 #stepBrake
        decisionSteer = 0

        if stepSteer<0.005 and stepSteer>-0.005 :
            decisionSteer == 0
        elif stepSteer<=-0.1:
            decisionSteer = stepSteer-0.015
        elif stepSteer>=0.1:
            decisionSteer = stepSteer+0.015
        else:
            decisionSteer = stepSteer


        # if stepAcceleration >= 0.0:
        #     decisionThrottle = stepAcceleration
        #     decisionBrake = 0.0
        # else:
        #     decisionBrake = stepAcceleration
        #     decisionThrottle = 0.0
        print("\n -----------------------------------------")
        print("decisionThrottle = "+ str(decisionThrottle))
        print("decisionBrake = "+ str(decisionBrake))
        print("decisionSteer = "+ str(decisionSteer))
    
        decision = carla.VehicleControl()
        decision.throttle = float(decisionThrottle)
        decision.steer = float(decisionSteer)
        decision.brake = float(decisionBrake)

        return decision



        #testing-----------------------------------------------------------------------------------------------


    def _trace_route(self, start_waypoint, end_waypoint):
        """
        This method sets up a global router and returns the
        optimal route from start_waypoint to end_waypoint.

            :param start_waypoint: initial position
            :param end_waypoint: final position
        """
        # Setting up global router
        if self._grp is None:
            dao = global_route_planner_dao.GlobalRoutePlannerDAO(
                self.world.get_map(), sampling_resolution=config.NEXT_WAYPOINT_DISTANCE)
            grp = global_route_planner.GlobalRoutePlanner(dao)
            grp.setup()
            self._grp = grp

        # Obtain route plan
        route = self._grp.trace_route(
            start_waypoint.transform.location,
            end_waypoint.transform.location)

        return route


    def getNextWaypoints(self, currentWaypoint, endWaypoint, number ):
        nextWaypoints = []
        route = self.routePlanner. _trace_route( currentWaypoint , endWaypoint)
        for i in range(number):
            if route[i] != None:
                nextWaypoints.append(route[i][0])

        return nextWaypoints

    def setStartSpawnPointByNumber(self, spawnNumber):
        self.spawnNumber = spawnNumber


    def setDestination(self, destinationLocation):
        self.endWaypoint = self.world.get_map().get_waypoint(destinationLocation)

    def isAtDestination(self):
        #TO-DO
        return False    
    