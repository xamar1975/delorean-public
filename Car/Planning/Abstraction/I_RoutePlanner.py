# abc == Abstract base class
from abc import ABC, abstractclassmethod

class I_RoutePlanner( ABC ): 
    
    @abstractclassmethod
    def setDestinationWaypoint( self ):
        pass

    @abstractclassmethod
    def getIndex( self ):
        pass

    @abstractclassmethod
    def getRoute( self ):
        pass
