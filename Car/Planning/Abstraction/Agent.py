# abc == Abstract base class
from abc import ABC, abstractclassmethod

class Agent( ABC ): 
    
    @abstractclassmethod
    def run_step( self ):
        pass