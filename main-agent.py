
print( "\n--Loading libraries :\n" )

import time
import config
import threading
import sys

sys.path.append( config.PATH_TO_EGG )
import carla
from Utility import DataCollector
from Utility import ExportToCSV

print( "All libraries loaded successfully !\n" )

print( "--Loading Classes :\n" )

from Car.Planning.Concrete_implementation.CarlaCarAgent import CarlaCarAgent
from Environments.Concrete_implementation.CarlaEnvironmentControler import CarlaEnvironmentControler
from Environments.Concrete_implementation.carlaClientConnector import CarlaClientConnector
from Utility import ImageProcessors
from Utility import DataType
# necessary to use Carla.XXXXX
import sys
sys.path.append( config.PATH_TO_EGG )
import carla

print( "All classes loaded successfully !\n" )

print( "--Starting client connection :\n" )

try:
    connector = CarlaClientConnector()
    
    if connector.testServerAvailability():

        client = connector.getClientConnection()
    
        agent = CarlaCarAgent( client )

        #To-Do
        spawnLocationNumber = 40
        endLocation = client.get_world().get_map().get_waypoint(carla.Location(x=65.517555, y=7.808434, z=-0.005950), True) #, 1 )  
        agent.drive(spawnLocationNumber, endLocation)

finally:
    if agent.removeAgent():
        print("Agent succesfully removed")
    else:
        print("Error while removing the agent")

    connector.terminateConnection()

    print( "Fin.\n" )
