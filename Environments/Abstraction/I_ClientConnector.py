# abc == Abstract base class
from abc import ABC, abstractclassmethod

class I_ClientConnector( ABC ): 

    @abstractclassmethod
    def getClientConnection( self ):
        pass
    
    @abstractclassmethod
    def terminateConnection( self ):
        pass 