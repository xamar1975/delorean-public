
import config
import os
import sys
import random

sys.path.append( config.PATH_TO_EGG )
import carla

from carla import VehicleLightState

VEHICULE_DISTANCE = 2.0 #constant, represent the distance npc vehicules will keep from each others

class CarlaEnvironmentControler:

    def __existInList( self, object, list ):

        isInList = False
        temporarySet = set( list )

        if object in temporarySet:
            isInList = True

        return isInList

    def __init__( self, carlaClient ):
        
        self.client = carlaClient
        self.world = self.client.get_world()
        self.pedestrians = []
        self.cars = []
        self.weather = "normal-day"
        self.weatherSnapshot = carla.World.get_weather(self.world)
        # TM is for managing vehicule-NPCs
        self.traffic_manager = self.client.get_trafficmanager( config.CARLA_TRAFFIC_MANAGER_PORT )
        self.traffic_manager.set_global_distance_to_leading_vehicle( VEHICULE_DISTANCE )

    def resetWorld( self ):
        """[ Is used to completely reset the simulation, 
             can be used to ensure complete removal of all actors. ]
        """

        self.client.reload_world()
        print('The world had been reset !\n')    

    def changeAllLightsToGreen( self ):
        """[ Is used to set all traffic-lights to green ]
        """
        
        trafficLights = self.world.get_actors().filter('traffic.traffic_light*')

        for light in trafficLights:

            light.set_state(carla.TrafficLightState.Green)
            light.freeze(True)
        
        print('All traffic-light were changed to green !\n')

    def changeAllLightsToRed( self ):
        """[ Is used to set all traffic-lights to Red ]
        """
        
        trafficLights = self.world.get_actors().filter('traffic.traffic_light*')

        for light in trafficLights:

            light.set_state(carla.TrafficLightState.Red)
            light.freeze(True)
        
        print('All traffic-light were changed to Red !\n')    


    def changeAllLightsToYellow( self ):
        """[ Is used to set all traffic-lights to Yellow ]
        """
        
        trafficLights = self.world.get_actors().filter('traffic.traffic_light*')

        for light in trafficLights:

            light.set_state(carla.TrafficLightState.Yellow)
            light.freeze(True)
        
        print('All traffic-light were changed to red !\n')

    def addPedestrians( self, numberAdded ):
        """[ Is used to add walking pedestrians to the simulation ]

        Args:
            numberAdded ([ Integer ]): [ number of pedestrian that the environnement controler will try to add,
                                         usually fewer pedestrians are actually added because of collisions, be mindful of that ]
        """

        SpawnActor = carla.command.SpawnActor
        percentagePedestriansRunning = 0.25
        percentagePedestriansCrossing = 0.25
        blueprintsWalkers = self.world.get_blueprint_library().filter( 'walker.pedestrian.*' )

        temporaryActorList = []
        all_id = []

        # 1. take all the random locations to spawn
        spawnPoints = []
        alreadyPickedSpawnPoints = []

        for i in range( numberAdded ):

            spawn_point = carla.Transform()
            loc = self.world.get_random_location_from_navigation()

            alreadyPickedSpawnPoints.append( loc )

            # reroll the spawn as long as the choice is already taken
            while self.__existInList( loc, alreadyPickedSpawnPoints ) :
                loc = self.world.get_random_location_from_navigation()

            spawn_point.location = loc
            spawnPoints.append( spawn_point )                

        # 2. we spawn the walker object
        batch = []
        walker_speed = []

        for spawn_point in spawnPoints:

            walker_bp = random.choice( blueprintsWalkers )

            # set as not invincible
            #if walker_bp.has_attribute('is_invincible'):
            #    walker_bp.set_attribute('is_invincible', 'false')

            # set the max speed
            if walker_bp.has_attribute('speed'):
                if (random.random() > percentagePedestriansRunning):
                    # walking
                    walker_speed.append( walker_bp.get_attribute('speed').recommended_values[1] )
                else:
                    # running
                    walker_speed.append( walker_bp.get_attribute('speed').recommended_values[2] )
            else:
                walker_speed.append(0.0)

            batch.append( SpawnActor(walker_bp, spawn_point) )
        
        results = self.client.apply_batch_sync( batch, True )

        for i in range( len(results) ):

            if results[i].error:
                print( results[i].error )
            else:
                temporaryActorList.append( {"id": results[i].actor_id} )                

        # 3. we spawn the walker controller
        batch = []
        walker_controller_bp = self.world.get_blueprint_library().find('controller.ai.walker')

        for i in range( len(temporaryActorList) ):
            batch.append(SpawnActor(walker_controller_bp, carla.Transform(), temporaryActorList[i]["id"]))

        results = self.client.apply_batch_sync(batch, True)

        for i in range( len(results) ):

            if results[i].error:
                print( results[i].error )
            else:
                temporaryActorList[i]["con"] = results[i].actor_id

        # 4. we put altogether the walkers and controllers id to get the objects from their id
        for i in range( len(temporaryActorList) ):
            
            all_id.append( temporaryActorList[i]["con"] )
            all_id.append( temporaryActorList[i]["id"] )

        self.pedestrians = self.world.get_actors( all_id )

        # wait for a tick to ensure client receives the last transform of the walkers we have just created
        self.world.wait_for_tick()
        #world.tick()

        # 5. initialize each controller and set target to walk to (list is [controler, actor, controller, actor ...])
        # set how many pedestrians can cross the road

        self.world.set_pedestrians_cross_factor( percentagePedestriansCrossing )

        for i in range(0, len(all_id), 2):

            # start walker
            self.pedestrians[i].start()

            # set walk to random point
            self.pedestrians[i].go_to_location( self.world.get_random_location_from_navigation() )
            
            # max speed
            self.pedestrians[i].set_max_speed( float(walker_speed[int(i/2)]) )

        actualNumberOfPedestrian = int( len(self.pedestrians)/2 )
        numberOfPedestrian = str( actualNumberOfPedestrian )
        print( numberOfPedestrian + ' pedestrians npcs were added !\n')    

    def removeAllPedestrians( self ):
        """[ Is used to remove all pedestrians from the simulation ]
        """

        self.client.apply_batch( [carla.command.DestroyActor(x) for x in self.pedestrians] )
        print('All pedestrians npcs were removed !\n')

    def addCars( self, numberAdded ):
        """[ Is used to add ai controled vehicules to the simulation ]

        Args:
            numberAdded ([ Integer ]): [ number of cars that the environnement controler will try to add,
                                         usually fewer cars are actually added because of collisions, be mindful of that ]
        """

        SpawnActor = carla.command.SpawnActor
        SetAutopilot = carla.command.SetAutopilot
        SetVehicleLightState = carla.command.SetVehicleLightState
        FutureActor = carla.command.FutureActor    
                
        blueprint_library = self.world.get_blueprint_library().filter( 'vehicle.*' )
        spawnPoints = self.world.get_map().get_spawn_points()
        numberOfSpawnPoints = len( spawnPoints )

        if numberAdded < numberOfSpawnPoints:
            random.shuffle(spawnPoints)
        else:
            print( 'Requested ' + str(numberAdded) + ' vehicles, but could only find ' + str(numberOfSpawnPoints) + ' spawn points\n' )
            numberAdded = numberOfSpawnPoints
        
        batch = []
        alreadyPickedSpawnPoints = []

        for i in range( numberAdded ):
            
            spawnPoint = random.choice( spawnPoints )
            alreadyPickedSpawnPoints.append( spawnPoint )

            blueprint = random.choice( blueprint_library )

            # reroll the spawn as long as the choice is already taken
            while self.__existInList(spawnPoint, alreadyPickedSpawnPoints) :
                spawnPoint = random.choice( spawnPoints )

            if blueprint.has_attribute('color'):
                color = random.choice(blueprint.get_attribute('color').recommended_values)
                blueprint.set_attribute('color', color)

            if blueprint.has_attribute('driver_id'):
                driver_id = random.choice(blueprint.get_attribute('driver_id').recommended_values)
                blueprint.set_attribute('driver_id', driver_id)

            blueprint.set_attribute( 'role_name', 'autopilot' )

            # prepare the light state of the cars to spawn
            light_state = VehicleLightState.NONE

            # if its night time all vehicules have lights on
            if self.weather == "normal-night" or self.weather == "rainy-night" :
                light_state = VehicleLightState.Position | VehicleLightState.LowBeam | VehicleLightState.LowBeam


            # spawn the cars and set their autopilot and light state all together
            batch.append( SpawnActor(blueprint, spawnPoint)
                .then( SetAutopilot(FutureActor, True, config.CARLA_TRAFFIC_MANAGER_PORT) )
                .then( SetVehicleLightState(FutureActor, light_state)) )

        results = self.client.apply_batch_sync( batch, True )

        vehicles_list = []

        for response in results:
            if response.error:
                print( response.error )
            else:
                vehicles_list.append(response.actor_id)

        self.cars = self.world.get_actors( vehicles_list )
        actualNumberOfCars = len( self.cars )      
        numberOfCars = str( actualNumberOfCars )
        print( numberOfCars + ' cars npcs were added !\n')    

    def removeAllCars( self ):
        """[ Is used to remove all npc cars from the simulation ]
        """

        self.client.apply_batch( [carla.command.DestroyActor(x) for x in self.cars] )        
        print('All cars npcs were removed !\n')

    def removeEverything( self ):
        """[ Is used to remove all pedestrians and all npc cars from the simulation ]
        """

        self.client.apply_batch( [carla.command.DestroyActor(x) for x in self.pedestrians] )
        self.client.apply_batch( [carla.command.DestroyActor(x) for x in self.cars] )   
            
        print('All pedestrians and cars npcs were removed !\n')

#----------------- Weather CheatSheet -----------------
# https://carla.readthedocs.io/en/latest/python_api/#carla.WeatherParameters

    def __displayWeatherAlreadySetMessage( self ):
        print('Current weather is already set to : ' + self.weather + ' !\n')
    
    def setToWeatherToNormalDayTime( self ):

        if self.weather != "normal-day" :
            newWeather = carla.WeatherParameters(
                cloudiness=0.0,
                precipitation=0.0,
                sun_altitude_angle=90.0,
                precipitation_deposits=0.0,
                fog_density=0.0,
                fog_distance=0.0,
                wetness=0.0)

            self.world.set_weather( newWeather )
            self.weather = "normal-day"
            print('Weather is now normal-day !\n')
        else :
            self.__displayWeatherAlreadySetMessage()        

    def setToWeatherToNormalEvening( self ):
        
        if self.weather != "normal-evening" :

            newWeather = carla.WeatherParameters(
                cloudiness=0.0,
                precipitation=0.0,
                sun_altitude_angle=10.0,
                precipitation_deposits=0.0,
                fog_density=0.0,
                fog_distance=0.0,
                wetness=0.0)

            self.world.set_weather( newWeather )
            self.weather = "normal-evening"
            print('Weather is now normal-evening !\n')
        else :
            self.__displayWeatherAlreadySetMessage()   

    def setToWeatherToNormalNight( self ):
        
        if self.weather != "normal-night" :
            newWeather = carla.WeatherParameters(
            cloudiness=0.0,
            precipitation=0.0,
            sun_altitude_angle=-90.0,
            precipitation_deposits=0.0,
            fog_density=0.0,
            fog_distance=0.0,
            wetness=0.0)

            self.world.set_weather( newWeather )
            self.weather = "normal-night"
            print('Weather is now normal-night !\n')
        else :
            self.__displayWeatherAlreadySetMessage()   

    def setToWeatherToRainyDayTime( self ):
        
        if self.weather != "rainy-day" :
            newWeather = carla.WeatherParameters(
                cloudiness=100.0,
                precipitation=100.0,
                sun_altitude_angle=90.0,
                precipitation_deposits=60.0,
                fog_density=70.0,
                fog_distance=15.0,
                wetness=0.25)

            self.world.set_weather( newWeather )
            self.weather = "rainy-day"
            print('Weather is now rainy-day !\n')
        else :
            self.__displayWeatherAlreadySetMessage()  
    
    def setToWeatherToRainyEvening( self ):

        if self.weather != "rainy-evening" : 
            newWeather = carla.WeatherParameters(
                cloudiness=100.0,
                precipitation=100.0,
                sun_altitude_angle=10.0,
                precipitation_deposits=60.0,
                fog_density=70.0,
                fog_distance=15.0,
                wetness=0.25)

            self.world.set_weather( newWeather )
            self.weather = "rainy-evening"
            print('Weather is now rainy-evening !\n')
        else :
            self.__displayWeatherAlreadySetMessage()  

    def setToWeatherToRainyNight( self ):

        if self.weather != "rainy-evening" :     
            newWeather = carla.WeatherParameters(
                cloudiness=100.0,
                precipitation=100.0,
                sun_altitude_angle=-90.0,
                precipitation_deposits=60.0,
                fog_density=70.0,
                fog_distance=15.0,
                wetness=0.25)

            self.world.set_weather( newWeather )
            self.weather = "rainy-night"
            print('Weather is now rainy-night !\n')        
        else :
            self.__displayWeatherAlreadySetMessage()  

    def setWeatherFromDict( self, values ):
        
        newWeather = carla.WeatherParameters(
            cloudiness = values['cloudiness'],
            precipitation = values['precipitation'],
            sun_altitude_angle = values['sun_altitude_angle'],
            precipitation_deposits = values['precipitation_deposits'],
            fog_density = values['fog_density'],
            fog_distance = values['fog_distance'],
            wetness = values['wetness'])
        
        self.world.set_weather(newWeather)
        self.weather = "custom-from-file"
        print('Weather is now a custom-from-file !\n')
    
    def exportWeatherSnapshotToDict(self):
        
        values = {}
        values['cloudiness'] = self.weatherSnapshot.cloudiness
        values['precipitation'] = self.weatherSnapshot.precipitation
        values['sun_altitude_angle'] = self.weatherSnapshot.sun_altitude_angle
        values['precipitation_deposits'] = self.weatherSnapshot.precipitation_deposits
        values['fog_density'] = self.weatherSnapshot.fog_density
        values['fog_distance'] = self.weatherSnapshot.fog_distance
        values['wetness'] = self.weatherSnapshot.wetness
        
        return values

    def reloadWeatherSnapshot( self ):
        self.world.set_weather(self.weatherSnapshot)
        
        