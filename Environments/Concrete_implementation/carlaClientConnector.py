#libraries
import sys
import config #config file
import platform
import subprocess
import socket

sys.path.append( config.PATH_TO_EGG )
import carla

from ..Abstraction import I_ClientConnector

class CarlaClientConnector( I_ClientConnector.I_ClientConnector ):

    def __init__( self ):
        
        self.connectionIsUp = False
        self.client = None

    # private class method
    def __initiateConnection( self ):

        self.client = carla.Client( config.CARLA_HOST_ADDRESS, config.CARLA_HOST_PORT )
        self.client.set_timeout( config.CARLA_CLIENT_TIMEOUT )
        self.connectionIsUp = True
    
    def testServerAvailability( self ):        

        isServerAbailable = False

        if self.__testHostAvailability() :
            print("\nHost is up !\n")            
            isServerAbailable = self.__testPortAvailability()
        else:
            print("\nHost is down !\n") 

        return isServerAbailable

    def __testHostAvailability( self ):        

        current_os = platform.system().lower()

        if current_os == "windows":
            parameter = "-n"
        else:
            parameter = "-c"        
        
        # Building the command. Ex: "ping -c 1 google.com"
        command = [ 'ping', parameter, '1', config.CARLA_HOST_ADDRESS ]

        isAvailable = subprocess.call(command) == 0

        return isAvailable

    def __testPortAvailability( self ):        

        a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        location = (config.CARLA_HOST_ADDRESS, config.CARLA_HOST_PORT)
        result_of_check = a_socket. connect_ex(location)

        isAvailable = result_of_check == 0

        if result_of_check == 0:
            print("CarlaServer is up !\n")
        else:
            print("CarlaServer is down !\n")
        
        a_socket.close()

        return isAvailable

    def getClientConnection( self ):

        if not self.connectionIsUp :
            self.__initiateConnection()

        return self.client

    def terminateConnection( self ):

        del self.client