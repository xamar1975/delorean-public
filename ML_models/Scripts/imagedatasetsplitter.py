import os
import shutil
import random


datasetFolder = 'dataset/'

#classes = [ 'road/', 'trafficred/', 'trafficgreen/', 'trafficyellow/' ]
classes = [ 'red/', 'nored/' ]
splitFolders = [ 'train/', 'test/', 'valid/' ]


print('\n-- Creating new folders : \n')

# making directory hierarchy
for folder in splitFolders:

    path = datasetFolder + folder
    os.mkdir( path )
    print( path +'\n')

    for currentClass in classes:

        path = datasetFolder + folder + currentClass
        os.mkdir( path )
        print( path +'\n')

# spliting the files
for currentClass in classes:

    path = datasetFolder + currentClass
    images = [file for file in os.listdir(path) if os.path.isfile(os.path.join(path, file))]

    totalImages = len( images )
    SplitPercent =  totalImages // 10

    print('-- Moving images from : ' + path + '\n')
    print(' Total : ' + str(totalImages))
    print(' 10%   : ' + str(SplitPercent) + '\n')
    print('-- Moving images to test/ and valid/\n')

    # we take 20% of the starting files and we move them in test/ and train/
    # we move 2 files per loop so we only need to account for half of the 20%
    for i in range( SplitPercent ):

        imagesStillAvailable = [file for file in os.listdir(path) if os.path.isfile(os.path.join(path, file))]

        randomFile = random.choice( imagesStillAvailable )
        randomFile = path + randomFile

        destination = datasetFolder + splitFolders[1] + currentClass
        result = shutil.move(randomFile, destination)
        print( result )

        imagesStillAvailable = [file for file in os.listdir(path) if os.path.isfile(os.path.join(path, file))]

        randomFile = random.choice( imagesStillAvailable )
        randomFile = path + randomFile

        destination = datasetFolder + splitFolders[2] + currentClass
        result = shutil.move(randomFile, destination)
        print( result )
        print( '\n' )

    print('-- Moving all remaining images to train/\n')
    #last we move all remaining files to training directory
    imagesStillAvailable = [file for file in os.listdir(path) if os.path.isfile(os.path.join(path, file))]
    destination = datasetFolder + splitFolders[0] + currentClass
    
    for image in imagesStillAvailable:

        imagePath = path + image
        result = shutil.move(imagePath, destination)
        print(result)

print('Fin.\n')


    
    
