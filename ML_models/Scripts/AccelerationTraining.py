# Import the dependencies you will need in this exercise
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from pandas import read_csv
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
import random
import sklearn.utils
import tensorflow as tf


#load the dataset --FINAL--
print('----------- Loading and randomizing data-------------')
path = 'data/Acceleration Data 04-09-2020.csv'
dataset = read_csv(path)
length = len(dataset)

#randomize order of dataset
dataset = dataset.reindex(np.random.permutation(dataset.index))

#split dataset into 80%, 10% and 10%
splitPercent = length//10
trainCutoff = length-splitPercent*2
validCutoff = length-splitPercent

quant_features = ['waypointDistance_y', 'waypointDistance_y']

# data = dataset
# scaled_features = {}
# for each in quant_features:
#     mean, std = data[each].mean(), data[each].std()
#     scaled_features[each] = [mean, std]
#     data.loc[:, each] = (data[each] - mean)/std
# print('------------quant features------------')
# print(data)


train = dataset[:trainCutoff]
valid = dataset[trainCutoff:validCutoff]
test = dataset[validCutoff:]

# x = context and y = Data to guess
y, x = train.values[:, :1], train.values[:, 1:] 
valid_y, valid_x = valid.values[:, :1], valid.values[:, 1:] 
test_y, test_x = test.values[:, :1], test.values[:, 1:] 

print('----------- Creating Model-------------')
model = Sequential()
model.add( Dense(36, activation="relu") )
model.add( Dense(8, activation="relu") )
model.add( Dense(1) )
model.add( Activation('tanh'))

print('----------- Compiling Model-------------')
model.compile(
    # Optimizer
    optimizer = tf.keras.optimizers.Adam(), 
    # Loss function to minimize
    loss= 'mse',
    metrics = ['accuracy']
)

history = model.fit( x, y, validation_data=(valid_x, valid_y) , epochs=50)

model.summary()

model.evaluate(test_x,  test_y, verbose=2)

 #And we plot the predictions against the real values
prediction = model.predict(valid_x)

for i in range(len(prediction)):
    print(str(prediction[i]) +'  '+ str(valid_y[i]))

