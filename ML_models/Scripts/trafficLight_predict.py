
print("\n-- Loading Imports :\n")

import numpy as np
import os
import tensorflow as tf

print("\n-- Version Check :\n")

print( "Tensorflow version : " + tf.__version__ )

print("\n-- GPU Check :\n")

device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
    print('GPU device not found')
else:
	print( 'Found GPU at: {}'.format(device_name) )

print("\n-- Model Load :\n")

loaded_model = tf.keras.models.load_model( 'trafficlights-20epochs-batch8.h5' )

print("\n-- Model Summary :\n")

loaded_model.summary()

print("\n-- Images Load :\n")

datasetFolder = 'dataset2020-09-03/'
testDir = datasetFolder + 'test/'
batch_size = 8
img_size = 600

image_gen = tf.keras.preprocessing.image.ImageDataGenerator( rescale = 1./255 )

test_generator = image_gen.flow_from_directory(
        testDir,
        target_size=( img_size, img_size ),
        batch_size=batch_size,
        class_mode='binary',
        shuffle=True,
        color_mode='rgb' )

print("\n-- Making Predictions :\n")

def resultDisplay( probability, filename ):
	print( filename + ":" )

	result = 0

	if probability < 0.5 :

		newProb = 100 - prob
		probString = str( round(newProb, 2) )
		print( "Nored : " + probString + "%\n" )

	else:

		probString = str( round(probability, 2) )
		print( "Red   : " + probString + "%\n" )

		result = 1
	
	split = filename.split('\\')

	print(split[0])


results = loaded_model.predict( test_generator )

numberOfFiles = len( test_generator.filenames )
print( "\nResults :\n" )

for i in range( numberOfFiles ):

	prob = float(results[i][0]) * 100
	fileName = test_generator.filenames[i]

	resultDisplay( prob, test_generator.filenames[i] )

