print( "\n--Loading libraries :\n" )
import time
import config
import cv2
import numpy as np
import threading
import sys
sys.path.append( config.PATH_TO_EGG )
import carla
from Utility import DataCollectorYaw
print( "All libraries loaded successfully !\n" )


print( "--Loading Classes :\n" )
from Car.Control.Concrete_implementation import CarlaCar
from Car.Sensors.Concrete_implementation import CarlaSensor
from Environments.Concrete_implementation.CarlaEnvironmentControler import CarlaEnvironmentControler
from Environments.Concrete_implementation.carlaClientConnector import CarlaClientConnector
from Utility import ImageProcessors
from Utility import DataType
print( "All classes loaded successfully !\n" )


try:
    connector = CarlaClientConnector()
    
    if connector.testServerAvailability():

        client = connector.getClientConnection()
        event = threading.Event()

        #print( "--Adding NPCs :\n" )
        #envControl = CarlaEnvironmentControler( client )
        #event.wait(3) 
        #numberOfPedestrian = 30
        #numberOfCars = 20
        #envControl.addCars(numberOfPedestrian)
        #envControl.addPedestrians(numberOfCars)
        runData = None
        print( "--Adding actors and main self-driven car :\n" )
        spawnPoint = 50 # 50 in front of the tunnel
        testCar = CarlaCar.CarlaCar()
        testCar.createCarInClient( client, config.CAR_MODEL, spawnPoint )
        
        for sensor in config.SENSORS:
            testCar.addSensor(
                sensor['TYPE'],
                sensor['X_POS'],
                sensor['Z_POS'],
                sensor['DATA_TYPE'], 
                sensor['ROLE'] )

        print( "--Testing car controls :\n" )
        testCar.startAutoPilot()
        event.wait(1)        
        
        carDataCollector = DataCollectorYaw.DataCollectorYaw(client, testCar)
        path = "data/"
        carDataCollector.CollectData( 100000, path )

finally:
    #### remove actors from client and close connection ####
    testCar.removeCarInClient()
    #envControl.removeEverything()
    connector.terminateConnection()

    print( "Fin.\n" )
