##########################################################################################################################
#                                 Use this file as an example to create your own config.py                               #
#                                For now it can be taken as is. But in future it might have to be customized             #
#                                depending of your environment.                                                          #
#                                                                                                                        #
#                                               Then rename the file to config.py                                        #
##########################################################################################################################

import os

from Utility import DataType

ML_MODELS = os.getcwd() + "\\ML_models"

PATH_TO_DECISION_MODEL = ML_MODELS + "\\decisions-noSpeed-50epochs-rand.h5"

CARLA_HOST_ADDRESS = "localhost"
CARLA_HOST_PORT = 2000
CARLA_TRAFFIC_MANAGER_PORT = 8000
CARLA_CLIENT_TIMEOUT = 10

SENSORS_IMAGE_WIDTH = 600
SENSORS_IMAGE_HEIGHT = 600
NEXT_WAYPOINT_DISTANCE = 2.0
STEP_TIME = 1.0

##########################################################################################################################
# --- Warning do not change car model without changing sensor X and Z positions accordingly ---
CAR_MODEL = 'model3'
SENSOR_X_POSITION = 2.5
SENSOR_Z_POSITION = 0.7

SENSORS = [
 {
 'TYPE': 'sensor.camera.rgb',
 'ROLE': 'front_car_image',
 'DATA_TYPE': DataType.PICTURE,
 'X_POS': 2.5,
 'Z_POS': 0.7
 },
 {
 'TYPE': 'sensor.other.gnss',
 'ROLE': 'gps',
 'DATA_TYPE': DataType.GPS,
 'X_POS': 2.5,
 'Z_POS': 0.7
 }
]